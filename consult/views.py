# -*- coding: utf-8 -*-
import json
from datetime import datetime
import traceback

from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Consultant, Chat, ChatUser
from .forms import ChatForm
from userapp.models import SUser


def message_add(request):
    s_user = SUser(request)
    user = request.user
    if user.is_authenticated:
        chat_user, c = ChatUser.objects.get_or_create(user=user)
    else:
        chat_user, c = ChatUser.objects.get_or_create(session_user=s_user.get_user)
    if request.method == "POST":
        form = ChatForm(request.POST)
        if form.is_valid():
            ch = form.save(commit=False)
            ch.chatid = chat_user
            ch.user = chat_user
            ch.save()
            
    result = {"result":True}
    js = json.dumps(result)
    return HttpResponse(js, content_type='application/json')


@csrf_exempt
def message_update(request):
    s_user = SUser(request).get_user
    user = request.user
    if user.is_authenticated:
        try:
            chat_user, c = ChatUser.objects.get_or_create(user=user)
        except ChatUser.MultipleObjectsReturned:
            ChatUser.objects.filter(user=user).delete()
            chat_user, c = ChatUser.objects.get_or_create(user=user)
            
    else:
        try:
            chat_user, c = ChatUser.objects.get_or_create(session_user=s_user)
        except ChatUser.MultipleObjectsReturned:
            ChatUser.objects.filter(session_user=s_user).delete()
            chat_user, c = ChatUser.objects.get_or_create(session_user=s_user)
    chat_user.added = datetime.now()
    chat_user.save(update_fields=['added',])
    
    chat_messages = Chat.objects.filter(chatid=chat_user).order_by("-added")[:20]
    try:
        js = json.dumps([(str(ch.message), str(ch.user.get_display_name()),
                          str(ch.get_display_time()), ch.is_user)
                         for ch in chat_messages])
    except:
        print(traceback.format_exc())
        print (str([(ch.message, str(ch.user.get_display_name()), ch.get_display_time(),
                      ch.is_user) for ch in chat_messages]))
        js = json.dumps(['Ошибка', '', '', False])
    return HttpResponse(js, content_type='application/json')
