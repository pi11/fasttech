from datetime import datetime, timedelta

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from consult.models import Chat

class Command(BaseCommand):
    help = 'Delete old messages in chat'

    def handle(self, *args, **options):
        old_date = datetime.now() - timedelta(days=60)
        Chat.objects.filter(added__lt=old_date).delete()
        print ("Chat cleared for last 60 days")
