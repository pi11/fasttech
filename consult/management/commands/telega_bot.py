import logging
import telegram
from datetime import datetime, timedelta

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from telegram.ext import Updater, CommandHandler, Job, RegexHandler
from telegram.ext import MessageHandler, Filters

from consult.models import Chat, Consultant, ChatUser, ErrorLog, ChatS

TOKEN = settings.TELEGRAM_TOKEN

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
    update.message.reply_text(u'Привет! Используйте /online для выхода в онлайн чат консультанта')


def alarm(bot, job):
    """Function to send the alarm message"""
    for error in ErrorLog.objects.filter(is_sended=False):
        print ("Error Message pk: %s" % error.pk)
        for admin in Consultant.objects.all():
            bot.sendMessage(admin.telegram_chat_id,
                            text="FASTEK.LINK Error Attention: %s" %\
                            error.message)
        error.is_sended = True
        error.save()
        
    for chm in Chat.objects.filter(is_sended=False):
        print ("Message pk: %s" % chm.pk)
        for admin in Consultant.objects.all():
            if chm.user.tuser:
                if admin != chm.user.tuser:
                    bot.sendMessage(admin.telegram_chat_id,
                                text="[%s] юзверю [%s]: %s" %\
                                    (chm.user.tuser.telegram_name,
                                     chm.chatid.pk, chm.message))

            else:
                custom_keyboard = [['ответить-%s' % chm.chatid.pk, ],
                                   ['последние чаты', ],]
                reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,
                                                resize_keyboard=True)
                bot.sendMessage(admin.telegram_chat_id,
                                text="Юзверь [%s]: %s\n/чат-%s" % (chm.chatid,
                                                                   chm.message,
                                                                   chm.chatid.pk),
                                reply_markup=reply_markup)
        chm.is_sended = True
        chm.save(update_fields=['is_sended'])

def online(bot, update, args, chat_data):
    """Adds user to online"""
    try:
        passw = int(args[0])
    except:
        print ("Can't parse password")
        return
    if passw != 42:
        return 
    chat_id = update.message.chat_id
    username = update.message.from_user['username']
    cons, c = Consultant.objects.get_or_create(telegram_name=username)
    cons.is_online = True
    cons.telegram_chat_id = chat_id
    cons.save()
    update.message.reply_text(u'Вы онлайн')

def offline(bot, update, chat_data):
    username = update.message.from_user['username']
    
    try:
        cons = Consultant.objects.get(telegram_name=username)
    except Consultant.DoesNotExist:
        pass
    else:
        cons.is_online = False
        cons.save()
        update.message.reply_text(u'Вы оффлайн')

def chat_hand(bot, update, groupdict):
    print (groupdict)
    chat_id = update.message.chat_id
    
    try:
        username = update.message.from_user['username']
        cons = Consultant.objects.get(telegram_name=username)
    except (KeyError, Consultant.DoesNotExist):
        print ("Error =)")
        return
    cons.is_online = True
    cons.telegram_chat_id = chat_id
    cons.save()
    
    chat_user = ChatUser.objects.get(pk=groupdict['chat_id'])
    
    if Chat.objects.filter(chatid=chat_user).count() == 0:
        update.message.reply_text(u'Юзверь не найден')
    cons_chat_user, c = ChatUser.objects.get_or_create(tuser=cons)
    chat = Chat.objects.get_or_create(chatid=chat_user, user=cons_chat_user,
                                      message=groupdict['text'])
    update.message.reply_text(u'>ok')



def chat_reply(bot, update, groupdict):
    print (groupdict)
    
    chat_id = update.message.chat_id
    
    try:
        username = update.message.from_user['username']
        cons = Consultant.objects.get(telegram_name=username)
    except (KeyError, Consultant.DoesNotExist):
        print ("Error. Consultant does not exists =)")
        return
    print ("Chatid: %s" % chat_id)
    chat_user = ChatUser.objects.get(pk=groupdict['chat_id'])

    
    if groupdict.get('anyway', '') == 'все':
        print("Все равно отвечаем")
    else:
        for ch in ChatS.objects.filter(chat_user=chat_user, is_active=True):
            print (ch.consultant, cons)
            if ch.consultant != cons:
                # another cons anwsering now...
                custom_keyboard = [['все равно ответить-%s' % groupdict['chat_id'], ],
                                   ['последние чаты', ],]
                reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,
                                                resize_keyboard=True)
                bot.sendMessage(chat_id,
                                text=u"Админ %s уже отвечает юзверю %s" % \
                                      (ch.consultant, chat_user),
                                reply_markup=reply_markup)

                return
        

    ChatS.objects.filter(consultant=cons).delete() # delete old chats
    
    chat_s, c = ChatS.objects.get_or_create(consultant=cons,
                                            chat_user=chat_user)
    
    for admin in Consultant.objects.all():
        if admin != cons:
            bot.sendMessage(admin.telegram_chat_id,
                            text="Админ %s отвечает юзверю %s" % \
                            (cons, chat_id))




    reply_markup = telegram.ReplyKeyboardRemove()

    bot.sendMessage(chat_id,
                    text=u'Введите текст. Все ваши сообщения будут отправлены юзверю %s' % chat_user,
                    reply_markup=reply_markup)
                

def chat_history(bot, update, groupdict):
    chat_id = update.message.chat_id

    reply_markup = telegram.ReplyKeyboardRemove()
    
    for t in Chat.objects.filter(chatid=groupdict['chat_id']).order_by("added"):
        text = "[%s %s]>\n[%s] \n\n" % (t.user, t.added, t.message)
        text += "============\n"
        bot.sendMessage(chat_id, text=text, reply_markup=reply_markup)
    
def message_hand(bot, update):
    try:
        username = update.message.from_user['username']
        cons = Consultant.objects.get(telegram_name=username)
    except (KeyError, Consultant.DoesNotExist):
        print ("Error. Consultant does not exists =)")
        return

    try:
        chatS = ChatS.objects.get(consultant=cons, is_active=True)
    except ChatS.DoesNotExist:
        custom_keyboard = [['последние чаты', ],]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,
                                            resize_keyboard=True)
        bot.sendMessage(cons.telegram_chat_id,
                        text=u"Вы никому не отвечаете, сначала выберете кому ответить",
                        reply_markup=reply_markup)
        return
    

    cons_chat_user = ChatUser.objects.get(tuser=cons)
    chat = Chat.objects.get_or_create(chatid=chatS.chat_user,
                                      user=cons_chat_user,
                                      message=update.message.text)
    update.message.reply_text(u'>ok')
    
    
def last_chats(bot, update):
    c_k = []
    text = ""
    q = Chat.objects.filter(added__gt=datetime.now()-timedelta(days=2))\
                    .order_by('-added')
    users = []
    for chat in q:
        if chat.user.tuser:
            pass # this is admin
        elif chat.user not in users:
            users.append(chat.user)
            last_text = chat.message
            c_k.append(['ответить-%s' % chat.user.pk,])
            text += "Юзверь: [%s], текст: [%s] \n" % (chat.user, last_text)
            text += "============\n"
    
    reply_markup = telegram.ReplyKeyboardMarkup(c_k,
                                            resize_keyboard=True)
    bot.sendMessage(update.message.chat_id,
                    text=text,
                    reply_markup=reply_markup)

    
def error(bot, update, error):
    logger.warning('Update "%s" caused error "%s"' % (update, error))


class Command(BaseCommand):
    help = 'Telega bot'

    def handle(self, *args, **options):
            updater = Updater(TOKEN)
            jobs = updater.job_queue
            due = 2
            job = Job(alarm, due, repeat=True)
            jobs.put(job)


            # Get the dispatcher to register handlers
            dp = updater.dispatcher

            # on different commands - answer in Telegram
            dp.add_handler(CommandHandler("start", start))

            dp.add_handler(RegexHandler("/(?P<chat_id>\d+)(?P<text>.*?)$",
                                        chat_hand, pass_groupdict=True))

            dp.add_handler(RegexHandler("ответить-(?P<chat_id>\d+)$",
                                        chat_reply, pass_groupdict=True))

            dp.add_handler(RegexHandler("чат-(?P<chat_id>\d+)$",
                                        chat_history, pass_groupdict=True))
            dp.add_handler(RegexHandler("/чат-(?P<chat_id>\d+)$",
                                        chat_history, pass_groupdict=True))

            dp.add_handler(RegexHandler("(?P<anyway>\w+) равно ответить-(?P<chat_id>\d+)$",
                                        chat_reply, pass_groupdict=True))

            dp.add_handler(RegexHandler("последние чаты", last_chats))

            dp.add_handler(CommandHandler("help", start))
            dp.add_handler(CommandHandler("online", online,
                                          pass_args=True,
                                          pass_chat_data=True))
            dp.add_handler(CommandHandler("offline", offline, pass_chat_data=True))

            dp.add_handler(MessageHandler(Filters.text, message_hand))

            # log all errors
            dp.add_error_handler(error)

            # Start the Bot
            updater.start_polling()


            
            # Block until you press Ctrl-C or the process receives SIGINT, SIGTERM or
            # SIGABRT. This should be used most of the time, since start_polling() is
            # non-blocking and will stop the bot gracefully.
            updater.idle()

