from django.db import models
from django.contrib.auth.models import User
from django.contrib.humanize.templatetags.humanize import naturaltime

from userapp.models import SessionUser


class Consultant(models.Model):
    telegram_name = models.CharField(max_length=50, unique=True)
    display_name = models.CharField(max_length=100)
    is_online = models.BooleanField(default=False)

    telegram_chat_id = models.IntegerField(null=True)
    
    def __str__(self):
        return self.telegram_name


class ChatUser(models.Model):
    user = models.ForeignKey(User, null=True, blank=True,
                             on_delete=models.CASCADE)
    tuser = models.ForeignKey(Consultant, null=True, blank=True,
                              on_delete=models.CASCADE)
    session_user = models.ForeignKey(SessionUser, null=True, blank=True,
                                     on_delete=models.CASCADE)
    added = models.DateTimeField(auto_now=True)

    def get_display_name(self):
        """Return  'display username' or Вы 
        for case user is not registered"""
        if self.user:
            return self.user.email
        elif self.tuser:
            return "Михаил Соколов"
            return self.tuser.display_name
        return "Вы"

    def get_internal_name(self):
        """Return  'display username' or Вы 
        for case user is not registered"""
        if self.user:
            return "%s (ID: %s)" % (self.user.email, self.pk)
        elif self.tuser:
            return "Михаил Соколов"
            return self.tuser.display_name
        return str(self.pk)

    def __str__(self):
        res = self.get_internal_name()
        return res


class Chat(models.Model):
    chatid = models.ForeignKey(ChatUser, related_name="chatid",
                               on_delete=models.CASCADE) # this field is not changed
    # beetwen messages, it is unique for "Chat"
    # so we can get messages for 1 chat only
    user = models.ForeignKey(ChatUser, on_delete=models.CASCADE) # this field changes beetwen
    # messages
    
    message = models.CharField(max_length=1000)
    added = models.DateTimeField(auto_now=True)
    is_sended = models.BooleanField(default=False)

    @property
    def is_user(self):
        if self.user.tuser:
            return False
        else:
            return True
        
    def __str__(self):
        return self.user

    def get_display_time(self):
        return naturaltime(self.added)


class ErrorLog(models.Model):
    message = models.TextField()
    is_sended = models.BooleanField(default=False)
    added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.message


class ChatS(models.Model):
    """"""
    consultant = models.ForeignKey(Consultant, on_delete=models.CASCADE)
    chat_user = models.ForeignKey(ChatUser, on_delete=models.CASCADE)
    added = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)
    
    def __str__(self):
        return str("%s->%s" % (self.sonsultant, self.chat_user))
