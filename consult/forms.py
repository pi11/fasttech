# -*- coding: utf-8 -*-

from django.conf import settings
from django import forms
from django.forms import HiddenInput, Textarea
from django.forms import ModelForm, Form
from django.contrib.auth.models import User
from django.utils import timezone

from .models import Chat



class ChatForm(forms.ModelForm):

    class Meta:
        model = Chat
        fields = ("message", )

