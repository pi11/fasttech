from django.contrib import admin

from consult.models import Consultant

class ConsultantAdmin(admin.ModelAdmin):
    list_display = ("telegram_name", "display_name")

    
admin.site.register(Consultant, ConsultantAdmin)
