from django.conf.urls import include, url

from consult.views import *
app_name = "consult"
urlpatterns = [
    url(r'^message/add/$', message_add, name='message_add'),
    url(r'^message/update/$', message_update, name='message_update'),
]
