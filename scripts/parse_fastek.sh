#!/bin/bash
 
dir=$(dirname $(which $0));
cd $dir
cd ..
source ../ve/bin/activate

mypidfile=/tmp/fastek.link-parse-fastek.pid
PID=$(<"$mypidfile")

if kill -0 $PID > /dev/null 2>&1;
then
   echo "Script is already runing, pid:[$PID]" >&2
   exit 1
fi

echo $$ > "$mypidfile"
python manage.py parse_pages 1000
python manage.py load_images 1000

# Ensure PID file is removed on program exit.
trap "rm -f -- '$mypidfile'" EXIT



