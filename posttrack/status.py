from django.conf import settings
from suds.client import Client
import time
import suds.bindings

#import logging
#logging.basicConfig(level=logging.INFO)
#logging.getLogger('suds.client').setLevel(logging.DEBUG)


def get_track_status(barcode):

    suds.bindings.binding.envns = ('SOAP-ENV',
    'http://www.w3.org/2003/05/soap-envelope')
    
    url = 'https://tracking.russianpost.ru/rtm34?wsdl'
    client = Client(
        url, headers={'Content-Type': 'application/soap+xml; charset=utf-8'})

    message = \
        """<?xml version="1.0" encoding="UTF-8"?>
                    <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:oper="http://russianpost.org/operationhistory" xmlns:data="http://russianpost.org/operationhistory/data" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
                    <soap:Header/>
                    <soap:Body>
                       <oper:getOperationHistory>
                          <data:OperationHistoryRequest>
                             <data:Barcode>""" + barcode + """</data:Barcode>
                             <data:MessageType>0</data:MessageType>
                             <data:Language>RUS</data:Language>
                          </data:OperationHistoryRequest>
                          <data:AuthorizationHeader soapenv:mustUnderstand="1">
                             <data:login>""" + settings.POST_LOGIN + """</data:login>
                             <data:password>""" + settings.POST_PASSWORD + """</data:password>
                          </data:AuthorizationHeader>
                       </oper:getOperationHistory>
                    </soap:Body>
                 </soap:Envelope>"""


    result = client.service.getOperationHistory(__inject={'msg': message})
    print (result)
    status = []
    try:
        if result.historyRecord:
            pass
    except AttributeError:
        print ("No information available")
    else:
        for rec in result.historyRecord:
            try:
                text = rec.AddressParameters.OperationAddress.Description
            except AttributeError:
                continue
            
            try:
                if rec.OperationParameters.OperAttr.Name:
                    pass
            except AttributeError:
                pass
            else:
                text = "%s, %s" % (text, rec.OperationParameters.OperAttr.Name)
            status.append({"date": str(rec.OperationParameters.OperDate),
                           "text":text})
    return status

