# -*- coding: utf-8 -*-
""" userapp views"""

from datetime import datetime
import hashlib
import json


from django.core.mail import send_mail
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout
from django.conf import settings
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.shortcuts import render
from django.contrib.admin.views.decorators import staff_member_required
from django.urls import reverse
from django.db.models import Q

from djangohelpers.utils import gen_url
from emailuser.models import EmailUserForm, manual_create_email_user
from userapp.models import *
from shop.models import ParseStat
from emailservice.models import EmailObject, SmsObject
from .forms import OrderForm, ShipmentForm, CouponForm, ItemSkuForm
from shop.functions import prepare_page

@staff_member_required
def new_orders(request):
    items = Order.objects.filter(is_payed=True, is_processed=False,
                                 is_deleted=False).order_by("-id")
    page, page_range = prepare_page(request, items)
    
    return render(request, "manager/new-orders.html",
                  {"page":page, "page_range":page_range, "new":True})

@staff_member_required
def orders(request):
    items = Order.objects.filter(is_done=False,
                                 is_deleted=False).order_by("-id")
    page, page_range = prepare_page(request, items)
    
    return render(request, "manager/new-orders.html",
                  {"page":page, "page_range":page_range, "new":False})

@staff_member_required
def old_orders(request):
    items = Order.objects.filter(is_payed=True,
                                 is_done=True).order_by("-id")
    page, page_range = prepare_page(request, items)
    
    return render(request, "manager/new-orders.html",
                  {"page":page, "page_range":page_range, "new":False,
                   "old":True})


@staff_member_required
def add_item_to_order(request, order_pk):
    order = get_object_or_404(Order, pk=order_pk)
    form = ItemSkuForm(request.POST)
    if form.is_valid():
        item = get_object_or_404(Item, sku=form.cleaned_data['sku'])
        order_item = OrderItem(item=item, order=order)
        order_item.save()
    return HttpResponseRedirect(reverse('manager:order_info',
                                        kwargs={"order_pk":order.pk}))

@staff_member_required
def remove_item_from_order(request, order_item_pk):
    order_item = get_object_or_404(OrderItem, pk=order_item_pk)
    order_item.delete()
    return HttpResponseRedirect(reverse('manager:order_info',
                                        kwargs={"order_pk":order_item.order.pk}))

@staff_member_required
def order_info(request, order_pk):
    order = get_object_or_404(Order, pk=order_pk)
    order_items = OrderItem.objects.filter(order=order)
    order_user = order.user.registereduser

    total_user_orders = Order.objects.filter(user=order.user,
                                             is_payed=True).count()
    if request.method == "POST":
        order_form = OrderForm(request.POST, instance=order)
        if order_form.is_valid():
            order_form.save()
    else:
        order_form = OrderForm(instance=order)
    sku_form = ItemSkuForm()
    return render(request, "manager/order.html",
                  {"order":order, "order_items":order_items,
                   "order_user":order_user, "order_form":order_form,
                   "total_user_orders": total_user_orders,
                   "sku_form":sku_form,
                   })


@staff_member_required
def email_list(request):
    items = EmailObject.objects.all().order_by("-id")
    page, page_range = prepare_page(request, items)
    return render(request, "manager/email-list.html",
                  {"page":page, "page_range":page_range})

@staff_member_required
def sms_list(request):
    items = SmsObject.objects.all().order_by("-id")
    page, page_range = prepare_page(request, items)
    return render(request, "manager/sms-list.html",
                  {"page":page, "page_range":page_range})

@staff_member_required
def new_reviews(request):
    reviews = Review.objects.filter(is_active=True, is_moderated=False)
    return render(request, "manager/reviews.html", {"reviews":reviews})

@staff_member_required
def review_approve(request):
    pk = request.GET.get("review_id", False)
    apr = request.GET.get("do", False)
    review = Review.objects.get(pk=pk)
    if apr == "approve":
        review.is_moderated = True
    if apr == "delete":
        review.is_active = False
    review.save()
    return HttpResponseRedirect(reverse('manager:new_reviews'))

@staff_member_required
def user_orders(request, user_pk):
    user = get_object_or_404(User, pk=user_pk)
    orders = Order.objects.filter(user__user=user, is_payed=True)
    return render(request, "manager/user-orders.html",
                  {"orders":orders, "o_user":user})

@staff_member_required
def shipment_add(request, order_pk):
    order = get_object_or_404(Order, pk=order_pk)
    order_items = OrderItem.objects.filter(order=order)
    if request.method == "POST":
        form = ShipmentForm(request.POST,  request.FILES, initial={"order":order})
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('manager:order_info', kwargs={"order_pk":order.pk}))
    else:
        form = ShipmentForm(initial={"order":order})
    
    return render(request, "manager/shipment-add.html", locals())

@staff_member_required
def shipment_info(request, shipment_pk):
    shipment = get_object_or_404(Shipment, pk=shipment_pk)
    try:
        post_tr = PostTracker.objects.get(shipment=shipment)
    except PostTracker.DoesNotExist:
        post_tr = False
    else:
        post_tr_items = PostTrackerItem.objects.filter(posttracker=post_tr).order_by("date")
    return render(request, "manager/shipment-info.html", locals())

@staff_member_required
def coupon_list(request):
    coupons = Coupon.objects.filter()
    return render(request, "manager/coupon-list.html", {"coupons":coupons})


@staff_member_required
def coupon_edit(request, coupon_pk=False):
    if coupon_pk:
        coupon = get_object_or_404(Coupon, pk=coupon_pk)
    else:
        coupon = None
    saved = request.GET.get("saved", False)

    if request.method == "POST":
        form = CouponForm(request.POST, instance=coupon)
        if form.is_valid():
            instance = form.save()
            return HttpResponseRedirect("%s?saved=True" % (reverse('manager:coupon_edit',
                                                                   kwargs={"coupon_pk":instance.pk})))
    else:
        form = CouponForm(instance=coupon)
    
    return render(request, "manager/coupon-edit.html",
                  {"form":form, "coupon":coupon, "saved":saved})

@staff_member_required
def parse_stat(request):
    last_parse = ParseStat.objects.filter().order_by("-id")[:10]
    return render(request, "manager/parse-stat.html", {"parse_stat":last_parse})


@staff_member_required
def new_video_reviews(request):
    video_reviews = VideoReview.objects.filter(is_active=True, is_moderated=False)
    page, page_range = prepare_page(request, video_reviews, 1)
    
    return render(request, "manager/video-reviews.html",
                  {"video_reviews":page, "page_range":page_range})

@staff_member_required
def video_review_approve(request):
    pk = request.GET.get("review_id", False)
    apr = request.GET.get("do", False)
    review = VideoReview.objects.get(pk=pk)
    if apr == "approve":
        review.is_moderated = True
        review.is_active = True
    if apr == "delete":
        review.is_moderated = True
        review.is_active = False
    review.save()

    try:
        video_review = VideoReview.objects.filter(is_active=True, is_moderated=False)[0]
    except IndexError:
        video_review = []
        
    return render(request, "manager/video-review-item.html",
                  {"review":video_review})

@staff_member_required
def index(request):
    reviews = Review.objects.filter(is_moderated=False,
                                    is_active=True).count()
    video_reviews = VideoReview.objects.filter(is_moderated=False,
                                               is_active=True).count()
    video_reviews_moderated = VideoReview.objects.filter(is_moderated=True).count()
    
    new_orders = Order.objects.filter(is_payed=True, is_processed=False,
                                      is_deleted=False).count()
    
    orders = Order.objects.filter(is_payed=True, is_done=False,
                                 is_deleted=False).count()
    old_orders = Order.objects.filter(is_payed=True, is_done=True).count()
    
    return render(request, "manager/index.html", locals())



@staff_member_required
def money_coupon_list(request):
    coupons = MoneyCoupon.objects.filter()
    return render(request, "manager/money-coupon-list.html", {"coupons":coupons})


@staff_member_required
def money_coupon_edit(request, coupon_pk=False):
    if coupon_pk:
        coupon = get_object_or_404(MoneyCoupon, pk=coupon_pk)
    else:
        coupon = None
    saved = request.GET.get("saved", False)

    if request.method == "POST":
        form = MoneyCouponForm(request.POST, instance=coupon)
        if form.is_valid():
            instance = form.save()
            return HttpResponseRedirect("%s?saved=True" % (reverse('manager:money_coupon_edit',
                                                                   kwargs={"coupon_pk":instance.pk})))
    else:
        form = MoneyCouponForm(instance=coupon)
    
    return render(request, "manager/money-coupon-edit.html",
                  {"form":form, "coupon":coupon, "saved":saved})

def search_order(request):
    q = request.GET.get('query', False)
    if not q or q == '':
        raise Http404

    shipments = Shipment.objects.filter(tracking__icontains=q)
    if len(shipments) > 0:
        orders = Order.objects.filter(pk__in=[sh.order_id for sh in shipments])
    else:
        try:
            q = int(q)
        except ValueError:
            orders = Order.objects.filter(
                Q(fasttech_order_url__icontains=q) | Q(coupon__title__icontains=q)|\
                Q(money_coupon__title__icontains=q)|Q(manager_remark__icontains=q)|\
                Q(user__registereduser__last_name__icontains=q)|Q(user__user__email__icontains=q))
        else:
            orders = Order.objects.filter(pk=q)
        
    page, page_range = prepare_page(request, orders)
    
    return render(request, "manager/new-orders.html",
                  {"page":page, "page_range":page_range, "new":False,
                   "q":q})
    
