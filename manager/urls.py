from django.conf.urls import include, url

from manager.views import *
app_name = "manager"
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^orders/new/$', new_orders, name='new_orders'),
    url(r'^user/orders/(?P<user_pk>\d+)/$', user_orders,
        name='user_orders'),

    url(r'^reviews/new/$', new_reviews, name='new_reviews'),
    url(r'^review_approve$', review_approve, name='review_approve'),

    url(r'^video-reviews/new/$', new_video_reviews, name='new_video_reviews'),
    url(r'^video-review_approve$', video_review_approve,
        name='video_review_approve'),

    url(r'^orders/old/$', old_orders, name='old_orders'),
    url(r'^orders/all/$', orders, name='orders'),
    url(r'^order/(?P<order_pk>\d+)/$', order_info, name='order_info'),
    url(r'^orders/add-item-to-order/(?P<order_pk>\d+)/', add_item_to_order,
        name='add_item_to_order'),
    url(r'^orders/remove-item-from-order/(?P<order_item_pk>\d+)/',
        remove_item_from_order, name='remove_item_from_order'),

    url(r'^emailservice/all/$', email_list, name='email_list'),
    url(r'^emailservice/sms/$', sms_list, name='sms_list'),

    url(r'^coupon/all/$', coupon_list, name='coupon_list'),
    url(r'^coupon/add/$', coupon_edit, name='coupon_edit'),
    url(r'^coupon/(?P<coupon_pk>\d+)/$', coupon_edit, name='coupon_edit'),

    url(r'^money-coupon/all/$', money_coupon_list, name='money_coupon_list'),
    url(r'^money-coupon/add/$', money_coupon_edit, name='money_coupon_edit'),
    url(r'^money-coupon/(?P<coupon_pk>\d+)/$', money_coupon_edit,
        name='money_coupon_edit'),


    
    url(r'order/(?P<order_pk>\d+)/shipment/add/$', shipment_add,
        name="shipment_add"),
    url(r'shipment/(?P<shipment_pk>\d+)/$', shipment_info, name="shipment_info"),

    url(r'^parse-stat$', parse_stat, name='parse_stat'),
    url(r'^search-order$', search_order, name='search_order'),
    
]
