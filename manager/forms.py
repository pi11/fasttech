# -*- coding: utf-8 -*-
import hashlib

from django.conf import settings
from django import forms
from django.forms import HiddenInput, Textarea
from django.forms import ModelForm, Form
from django.contrib.auth.models import User
from django.contrib.admin import widgets
from userapp.models import Order, RegisteredUser, Shipment, OrderItem, Coupon, \
    MoneyCoupon


class ShipmentForm(forms.ModelForm):
    class Meta:
        model = Shipment
        fields = ("tracking", "orderitem", "photo", "order")
        widgets = {'order': forms.HiddenInput()}

    def __init__(self, *args, **kwargs):
        super(ShipmentForm, self).__init__(*args, **kwargs)
        self.fields['orderitem'].queryset = OrderItem.objects.filter(\
                                            order=self.initial['order'])
        
class CouponForm(forms.ModelForm):
    class Meta:
        model = Coupon
        exclude = ("added", "used_count",)

    def __init__(self, *args, **kwargs):
        super(CouponForm, self).__init__(*args, **kwargs)
        self.fields['start_date'].widget = forms.TextInput(\
                                        attrs={"class":"date_field"})
        self.fields['end_date'].widget = forms.TextInput(\
                                        attrs={"class":"date_field"})
        

class MoneyCouponForm(forms.ModelForm):
    class Meta:
        model = MoneyCoupon
        exclude = ("added", )


class OrderForm(forms.ModelForm):

    class Meta:
        model = Order
        fields = ("is_done", "is_sended", "is_processed",  "is_payed",
                  "estimated_send_date", "manager_remark", "is_deleted",
                  "fasttech_order_url", )

    def __init__(self, *args, **kwargs):
        super(OrderForm, self).__init__(*args, **kwargs)
        self.fields['estimated_send_date'].widget = forms.TextInput(\
                                            attrs={"class":"date_field"})

class ItemSkuForm(forms.Form):
    sku = forms.CharField(label="SKU", max_length=30)
    
