from django.db import models

class EmailObject(models.Model):
    to = models.CharField(max_length=150, verbose_name="User email")
    subject = models.CharField(max_length=150)
    text = models.CharField(max_length=2500)
    publication_date = models.DateTimeField(auto_now_add=True)
    retries = models.IntegerField(default=0)
    is_sended = models.BooleanField(default=False)
    send_date = models.DateTimeField(null=True, blank=True)
    error = models.TextField(null=True, blank=True)
    priority = models.IntegerField(default=5)
    
    def __str__(self):
        return self.to

        
class SmsObject(models.Model):
    to = models.CharField(max_length=150, verbose_name="User phone")
    text = models.CharField(max_length=500)
    publication_date = models.DateTimeField(auto_now_add=True)
    retries = models.IntegerField(default=0)
    is_sended = models.BooleanField(default=False)
    send_date = models.DateTimeField(null=True, blank=True)
    error = models.TextField(null=True, blank=True)
    priority = models.IntegerField(default=5)
    
    def __str__(self):
        return self.to


