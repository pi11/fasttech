from django.contrib import admin

from .models import EmailObject

class EmailObjectAdmin(admin.ModelAdmin):
    pass

admin.site.register(EmailObject, EmailObjectAdmin)
