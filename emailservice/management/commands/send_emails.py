import sys
import time
import traceback
from datetime import datetime
from django.core.management.base import BaseCommand, CommandError
from django.core.mail import send_mail
from django.conf import settings

from consult.models import ErrorLog
from emailservice.models import EmailObject

class Command(BaseCommand):
    help = 'Parse fasttech.com'

    def process_emails(self):
        for e in EmailObject.objects.filter(is_sended=False, retries__lt=5).order_by("priority", "retries"):
            try:
                send_mail(e.subject, e.text, settings.SUPPORT_EMAIL, [e.to,])
            except:
                error = traceback.format_exc()
                full_error = "Can't send message. Tries: %s, error was: %s" % (e.retries, error)
                print(full_error)
                e.error = error
                e.retries += 1
                e.save()
                n_er, c = ErrorLog.objects.get_or_create(message=full_error)
                time.sleep(e.retries*2)
            else:
                print("Sended: %s" % e.to)
                e.is_sended = True
                e.send_date = datetime.now()
                e.save()
                time.sleep(3)
        
    def handle(self, *args, **options):
        while 1:
            self.process_emails()
            time.sleep(5)
            
