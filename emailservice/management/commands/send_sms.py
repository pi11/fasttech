import sys
import time
import traceback
import requests

from datetime import datetime
from django.core.management.base import BaseCommand, CommandError
from django.core.mail import send_mail
from django.conf import settings


from consult.models import ErrorLog
from emailservice.models import SmsObject

class Command(BaseCommand):
    help = 'Parse fasttech.com'

    def process_emails(self):
        def send_sms(text, phone):
            url = "https://smsc.ru/sys/send.php"
            data = {"login": settings.SMS_LOGIN, "psw": settings.SMS_PASSWORD,
                    "phones": phone, "mes": text, "charset":"utf-8",
                    "sender":"FASTEK"}
            r = requests.post(url, data=data)
            return 
        
        for e in SmsObject.objects.filter(is_sended=False, retries__lt=5,
                                            ).order_by("priority", "retries"):
            try:
                print ("Sending: %s: %s" % (e.text, e.to))
                send_sms(e.text, e.to)
            except:
                error = traceback.format_exc()
                full_error = "Can't send message. Tries: %s, error was: %s" % (e.retries, error)
                e.error = error
                e.retries += 1
                e.save()
                n_er, c = ErrorLog.objects.get_or_create(message=full_error)
                print (error)
                time.sleep(e.retries*2)
            else:
                e.is_sended = True
                e.send_date = datetime.now()
                e.save()
                time.sleep(3)
        
    def handle(self, *args, **options):
        while 1:
            self.process_emails()
            time.sleep(5)
            
