#-*- coding: utf-8 -*-

from django.contrib import admin

from .models import *

class CategoryAdmin(admin.ModelAdmin):
    list_display = ("name_rus", "order", "count", "is_active")

class ProductTypeAdmin(admin.ModelAdmin):
    pass

class ParseStatAdmin(admin.ModelAdmin):
    list_display = ("new_titles", "new_items", "parse_date")

class SearchStatAdmin(admin.ModelAdmin):
    list_display = ("query", "count")

class TopCategoryAdmin(admin.ModelAdmin):
    pass

class ColorAdmin(admin.ModelAdmin):
    pass

class MaterialAdmin(admin.ModelAdmin):
    pass

class PackageTypeAdmin(admin.ModelAdmin):
    pass

class ItemPropertyTypeAdmin(admin.ModelAdmin):
    pass

class ItemPropertyNameAdmin(admin.ModelAdmin):
    pass

class ItemPropertyValueAdmin(admin.ModelAdmin):
    pass

class ItemPropertyAdmin(admin.ModelAdmin):
    pass

    
class ItemAdmin(admin.ModelAdmin):
    list_filter = ("is_active", "home_page")
    readonly_fields = ("sku", "get_url")
    search_fields = ("sku", "title", "title_rus")
    list_display = ("title", "get_thumb_admin", "wilson_score", 
                    "title_rus", "is_active", "price", "added", "total_votes")
                
    


class OptionAdmin(admin.ModelAdmin):
    pass

class StockAdmin(admin.ModelAdmin):
    pass

class TranslateFixAdmin(admin.ModelAdmin):
    list_display = ("eng", "rus")

class BackTranslateAdmin(admin.ModelAdmin):
    list_display = ("rus", "eng")

    
admin.site.register(TopCategory, TopCategoryAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(BackTranslate, BackTranslateAdmin)
admin.site.register(TranslateFix, TranslateFixAdmin)

admin.site.register(ProductType, ProductTypeAdmin)
admin.site.register(Color, ColorAdmin)
admin.site.register(Material, MaterialAdmin)
admin.site.register(PackageType, PackageTypeAdmin)
admin.site.register(ItemPropertyType, ItemPropertyTypeAdmin)
admin.site.register(ItemPropertyName, ItemPropertyNameAdmin)
admin.site.register(ItemPropertyValue, ItemPropertyValueAdmin)
admin.site.register(ItemProperty, ItemPropertyAdmin)
admin.site.register(Item, ItemAdmin)
admin.site.register(Option, OptionAdmin)
admin.site.register(Stock, StockAdmin)
admin.site.register(ParseStat, ParseStatAdmin)

admin.site.register(SearchStat, SearchStatAdmin)

