import re
import sys
from django.core.management.base import BaseCommand, CommandError
from shop.models import *

class Command(BaseCommand):
    help = 'Parse fasttech.com'

    def handle(self, *args, **options):
        j = 0
        q = Item.objects.filter(title_rus__isnull=False)
        total = q.count()
        for i in q:
            j += 1
            if j % 20 == 0:
                self.stdout.write(".", ending="")
                sys.stdout.flush()

            if j % 1000 == 0:
                print("%s of %s completed" % (j, total))

            for btr in BackTranslate.objects.all():
                #print (">", btr.rus)
                retr = re.compile(r'\b%s\b' % btr.rus, re.I+re.UNICODE)
                text = retr.sub(btr.eng, i.title_rus)
                if text != i.title_rus:
                    print("Changed: %s -> %s" % (i.title_rus, text))
                    i.title_rus = text
                    i.save(update_fields=['title_rus'])
    
        self.stdout.write(self.style.SUCCESS('Done'))
