import requests

from datetime import datetime, timedelta

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from userapp.models import PostTracker, PostTrackerItem, Shipment
from emailservice.models import EmailObject, SmsObject



def get_track(track):
    api_url = "https://api.track24.ru/tracking.json.php?apiKey=%s&domain=fastek.link&pretty=true&code=%s" % (settings.TRACK24_API, track)
    r = requests.get(api_url)
    data = r.json()
    if data['status'] == 'ok':
        return data['data']
    else:
        return False


class Command(BaseCommand):
    help = 'Parse fasttech.com'

    def handle(self, *args, **options):
        print(get_track(''))
        
        dt = datetime.now() - timedelta(hours=8)
        for sh in Shipment.objects.filter(is_recieved=False):
            if sh.order.is_done:
                sh.is_recieved = True
                sh.is_delivered = True
                sh.save()
                
        for sh in Shipment.objects.filter(is_recieved=False, last_check__lt=dt,
                                          tracking__isnull=False,
                                          order__is_deleted=False)[:5]:
            
            print ("Shipment: %s" % sh)
            sh.last_check = datetime.now()
            sh.save(update_fields=['last_check',])
            
            if sh.tracking != "":
                print ("Checking order #%s, track: %s" % (sh.order.pk, sh.tracking))
            else:
                print ("No track number!, shupment: %s" % sh.pk)
                continue

            t_status = get_track(sh.tracking)
            if not t_status:
                print ("No status")
                continue

            #print(t_status)
            #print ("=" * 20)
            pt, c = PostTracker.objects.get_or_create(shipment=sh)
            new = False
            for el in t_status['events']:
                print(el)
                if el['serviceName'] == 'Track24':
                    if el['operationType'] == 'Вероятно, посылка еще не отправлена.':
                        new = True
                    
                    continue # skipp track24 info messages
                pti, c = PostTrackerItem.objects.get_or_create(
                    posttracker=pt, date=el['operationDateTime'], status=el['operationAttribute'],
                    info=el['operationAttributeInformation'])
                last_ = el['operationAttribute']
                last_info = el['operationAttributeInformation']
            if new:
                continue # the shipment is new or very old and not exists
            
            if last_ != pt.last_status:
                print (last_)
                pt.last_status = last_
                pt.save(update_fields=['last_status',])
                sh.track_status = last_
                sh.save(update_fields=['track_status',])

                
                text = "Информация по заказу #%s на сайте fastek.link\n\n" % sh.order.pk
                text += "Статус почтового отправления %s обновился \n" % sh.tracking
                text += "Текущий статус: %s\n" % last_
                text += "\n----------------------\n"
                text += last_info
                text = text.replace('&lt;br&gt', '\n')
                
                if not "Вручение адресату" in last_:
                    eo, c = EmailObject.objects.get_or_create(
                        subject="Статус отправления %s (fastek.link)" % sh.tracking,
                        text=text, to=sh.order.user.user.email)

            if "Прибыло в место вручения" in last_:
                pt.is_delivered = True
                pt.delivery_date = datetime.now()
                pt.save(update_fields=['is_delivered', 'delivery_date'])
                sh.is_delivered = True
                sh.save(update_fields=["is_delivered",])
                
                so, c = SmsObject.objects.get_or_create(
                    to=sh.order.user.registereduser.get_phone(),
                    text=("FASTEK.LINK: Посылка прибыла в место вручения, можно забирать.\n"
                          "Трэк номер: %s" % (sh.tracking)))

            if "Вручение адресату" in last_:
                pt.is_recieved = True
                pt.save(update_fields=['is_recieved'])
                sh.is_recieved = True
                sh.save(update_fields=['is_recieved'])
                sh.order.is_done = True
                sh.order.save(update_fields=['is_done',])
                
