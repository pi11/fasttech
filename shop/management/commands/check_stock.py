import sys
from django.core.management.base import BaseCommand, CommandError
from shop.models import *
from pyquery import PyQuery as pq
from djangohelpers.url import load_url
from api.views import check_stock_f

class Command(BaseCommand):
    help = 'Parse fasttech.com, and check stocks'

    def handle(self, *args, **options):
        total = 500
        j = 0
        for i in Item.objects.filter().order_by("stock_last_check")[:total]:
            j += 1
            if j % 20 == 0:
                self.stdout.write(".", ending="")
                sys.stdout.flush()

            if j % 1000 == 0:
                print("%s of %s completed" % (j, total))
            i.save()
            res = check_stock_f(i.pk)
        self.stdout.write(self.style.SUCCESS('Done'))
