# parse fasttech pages

from datetime import datetime
from django.core.management.base import BaseCommand, CommandError
from shop.models import *
from pyquery import PyQuery as pq
from djangohelpers.url import load_url
from django.utils import timezone
from django.db.models import Count

class Command(BaseCommand):
    help = 'Parse fasttech.com'

    def add_arguments(self, parser):
        parser.add_argument('pages_count', type=int)
        
    def handle(self, *args, **options):
        def parse_url(url, NewItem):
            new_titles = 0
            new_items = 0
            self.stdout.write(self.style.SUCCESS('Parsing: %s' % url))
            data = load_url(url, headers={"User-agent":"Mozilla/5.0 (X11; Linux x86_64; rv:6.0.1) Gecko/20110831 conkeror/0.9.3"})
            if not data:
                self.stdout.write(self.style.WARNING('Error loading page!'))
            else:
                if not "ProductOptions" in str(data): # LISTing removed?
                    print ("Listing removed?")
                    fturl.delete()
                    return 0, 0
                parsed = pq(data)
                parsed.make_links_absolute(base_url='http://www.fasttech.com')
                photos = parsed('#ThumbnailsPanel').items('a')
                i = 0
                for ph in photos:
                    photo, c = Photo.objects.get_or_create(url=ph.attr('href'))
                    NewItem.photos.add(photo)
                    if c:
                        i += 1
            return i

        q = Item.objects.annotate(ph_num=Count('photos')).filter(ph_num__lt=1)
       # print (q.query)
        for item in q:
            print ("Added %s photos" % parse_url(item.url.url, item))
        self.stdout.write(self.style.SUCCESS('Done'))
