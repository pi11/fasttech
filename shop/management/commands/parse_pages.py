# parse fasttech pages

from django.core.mail import mail_admins

from datetime import datetime
from django.core.management.base import BaseCommand, CommandError
from shop.models import *
from pyquery import PyQuery as pq
from djangohelpers.url import load_url
from django.utils import timezone
import cfscrape

class Command(BaseCommand):
    help = 'Parse fasttech.com'

    def add_arguments(self, parser):
        parser.add_argument('pages_count', type=int)
        
    def handle(self, *args, **options):
        def parse_url(fturl):
            new_titles = 0
            new_items = 0
            fturl.parsed_at = timezone.now()
            fturl.save(update_fields=['parsed_at'])
            url = fturl.url
            self.stdout.write(self.style.SUCCESS('Parsing: %s' % url))
            scraper = cfscrape.CloudflareScraper()

            r = scraper.get(url)
            if r.status_code != 200:
                self.stdout.write(self.style.WARNING('Error loading page!'))
            else:
                data = r.content
                if not "ProductOptions" in str(data): # LISTing removed?
                    print ("Listing removed?")
                    fturl.is_removed = True
                    fturl.save()
                    return 0, 0
                parsed = pq(data)
                parsed.make_links_absolute(base_url='http://www.fasttech.com')
                try:
                    reviewCount = int(parsed("span[itemprop='reviewCount']").text())
                except ValueError:
                    reviewCount = 0
                try:
                    ratingValue = float(parsed("span[itemprop='ratingValue']").text())
                except ValueError:
                    ratingValue = 0

                print("RATE:%s, %s" % (reviewCount, ratingValue))

                category_name = parsed('.Breadcrumb a').eq(2).text()
                category, c = Category.objects.get_or_create(name=category_name)
                
                title = parsed("#content_ProductTitle").text()
                sub_title = parsed("#content_Tagline").text()
                price = parsed("#content_Price").text().replace(' ', '').replace('$', '').strip()
                stock_name = parsed('.ProductOptions').find('meta[itemprop="availability"]').attr('content')
                
                stock, c = Stock.objects.get_or_create(name=stock_name)
                photos = parsed('#ThumbnailsPanel').items('a')

                description = parsed('.ProductDescriptions:first').html()
                if 'love to help!' in description:
                    description = ""

                try:
                    NewItem = Item.objects.filter(url=fturl)[0]
                    if NewItem.title != title:
                        new_titles += 1
                        NewItem.is_title_changed = True
                    if NewItem.sub_title != sub_title:
                        NewItem.is_subtitle_changed = True
                        
                except IndexError:
                    NewItem = Item(title=title, url=fturl)
                    new_items += 1


                NewItem.title = title
                NewItem.sub_title = sub_title
                NewItem.price = float(price)
                NewItem.stock = stock
                NewItem.category = category
                NewItem.total_votes = reviewCount
                NewItem.rate = ratingValue
                NewItem.save()
                #print NewItem.description
                
                for ph in photos:
                    photo, c = Photo.objects.get_or_create(url=ph.attr('href'))
                    NewItem.photos.add(photo)
                
                    
                options = parsed('.AttributesTable').items('tr')
                old_item = False

                # parse related items
                related = parsed('#Products_Grid').items('a.JsonAddToCart')
                for r in related:
                    sku = r.attr('data-sku')
                    try:
                        r_item = Item.objects.get(sku=sku)
                    except Item.DoesNotExist:
                        r_item = False
                    try:
                        ri, c = ItemRelatedSKU.objects.get_or_create(
                            item=NewItem,
                            related_sku=sku)
                    except ItemRelatedSKU.MultipleObjectsReturned:
                        ItemRelatedSKU.objects.filter(item=NewItem, 
                                                      related_sku=sku).delete()
                        ri, c = ItemRelatedSKU.objects.get_or_create(
                            item=NewItem,
                            related_sku=sku)
                        
                    if r_item:
                        ri.related_item = r_item
                        ri.save()

                
                for option in options:
                    #print option('td:first').attr('class')
                    if option('td:first').attr('class') == 'AttributesTableRowGroupTitle':
                        option_group = option('td:first').text()
                        #print "-----", option_group, "-------"
                        option_type, c = ItemPropertyType.objects.get_or_create(
                            name=option_group)
                        
                    else:
                        option_name = option('td:first').text().strip()
                        option_value = option('td:last').text().strip()
                        #print option_name, ':', option_value
                        is_option_added = False
                        if option_name.lower() == "sku":
                            sku = option_value
                            print(sku)
                            is_option_added = True
                            try:
                                obj = Item.objects.get(sku=sku)
                            except Item.DoesNotExist:
                                NewItem.sku = sku
                                NewItem.save(update_fields=['sku'])
                            #else:
                                
                                #old_item = True
                            
                        if option_name.lower() == "date listed":
                            date_listed = datetime.strptime(option_value, "%m/%d/%Y")
                            is_option_added = True
                            NewItem.date_listed = date_listed
                            
                        if option_name.lower() == "product type":
                            product_type, c = ProductType.objects.get_or_create(
                                name=option_value)
                            NewItem.product_type = product_type
                            is_option_added = True

                        if option_name.lower() == "color":
                            color, c = Color.objects.get_or_create(name=option_value)
                            is_option_added = True
                            NewItem.color = color
                            
                        if option_name.lower() == "material":
                            material, c = Material.objects.get_or_create(
                                name=option_value)
                            is_option_added = True
                            NewItem.material = material
                            
                        if option_name.lower() == "package type":
                            package_type, c = PackageType.objects.get_or_create(
                                name=option_value)
                            is_option_added = True
                            NewItem.package_type = package_type
                            
                        if not is_option_added:
                            item_name, c = ItemPropertyName.objects.get_or_create(
                                name=option_name)
                            item_value, c = ItemPropertyValue.objects.get_or_create(
                                name=option_value)
                            item_property, c = ItemProperty.objects.get_or_create(
                                property_type=option_type, 
                                property_name=item_name, 
                                property_value=item_value)
                            a = Item.objects.filter(
                                pk=NewItem.pk, 
                                item_properties__pk=item_property.pk).count()
                            if a == 0:
                                print ("Added")
                                NewItem.item_properties.add(item_property)
                            else:
                                print ("NO %s" % a)
                            
                print(title, sub_title, category, price, sku, date_listed)
                #if old_item:
                #    print "DELETING"
                #    print fturl.url
                #    print NewItem.url.url
                #    NewItem.delete()
                #else:
                NewItem.save()
                idesc, c = ItemDescription.objects.get_or_create(item=NewItem)
                if not c:
                    if idesc.description != description:
                        idesc.is_description_changed = True
                idesc.description = description
                    
                idesc.save()

                fturl.is_parsed = True
                fturl.save()
            return new_titles, new_items
            
        new_titles = 0
        new_items = 0
        for fturl in FasttechUrl.objects.filter(
                parsed_at__isnull=True,
                is_removed=False).order_by("-id")[:options['pages_count']]:
            t, i = parse_url(fturl)
            new_items += i
            new_titles += t
        for fturl in FasttechUrl.objects.filter(
                is_removed=False).order_by("parsed_at")[:options['pages_count']]:
            t, i = parse_url(fturl)
            new_items += i
            new_titles += t

            
        ps = ParseStat(new_titles=new_titles, new_items=new_items)
        ps.save()
        total = 0
        for ps_ in ParseStat.objects.filter().order_by("-parse_date")[:10]:
            total += ps_.new_items
            total += ps_.new_titles
        if total == 0:
            mail_admins('Warning: Fasttech.com parsing problem',
                        'Last 10 times scanner did not found new items, please check')
        self.stdout.write(self.style.SUCCESS('Done'))
