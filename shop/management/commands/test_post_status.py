from django.core.management.base import BaseCommand, CommandError
from userapp.models import PostTracker, PostTrackerItem, Shipment
from emailservice.models import EmailObject, SmsObject
from posttrack.status import get_track_status

from datetime import datetime, timedelta

class Command(BaseCommand):
    help = 'Parse fasttech.com'

    def add_arguments(self, parser):
        parser.add_argument('code')

    def handle(self, *args, **options):
        code = options['code']
        t = get_track_status(code)
        print (t)
