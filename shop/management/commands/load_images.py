# -*- coding: utf-8 -*-

import sys
import os
import random
import requests
from django.core.management.base import BaseCommand, CommandError
from shop.models import *
#from djangohelpers.url import load_url
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from django.db.models import Q

class Command(BaseCommand):
    help = 'Download images from fasttech'

    def add_arguments(self, parser):
        parser.add_argument('count', type=int)

    def handle(self, *args, **options):
        self.stdout.write('\n====================================')
        self.stdout.write('Downloading images from fasttech.com')
        count = options['count']
        photos = Photo.objects.filter(Q(image='') | Q(image__isnull=True)).order_by("id")[:count]
        j = 0
        for ph in photos:
            headers = {'user-agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0'}
            r = requests.get(ph.url)
            img_temp = NamedTemporaryFile(delete=True)
            img_temp.write(r.content)            
            img_temp.flush()
            filename = "%s/%s.jpg" % (settings.MEDIA_ROOT, ph.pk)
            ph.image.save(filename, File(img_temp))            
            ph.save()
            self.stdout.write('.', ending='')
            sys.stdout.flush()

            j += 1
            if j % 50 == 0:
                print(" %s of %s images downloaded;" % (j, count))

        self.stdout.write(self.style.SUCCESS('Done'))

