import sys
import re
from functools import reduce
from pyquery import PyQuery as pq
from yandex_translate import YandexTranslate

from django.core.management.base import BaseCommand, CommandError
from django.db.models.functions import Length
from django.conf import settings
from django.utils import timezone

from shop.models import *

class Command(BaseCommand):
    help = 'Parse fasttech.com'

    def handle(self, *args, **options):
        def CL(str):
            return reduce(getattr, str.split("."), sys.modules[__name__])

        def my_translate(text):
            """Prepare some special words"""

            translate = YandexTranslate(settings.YANDEX_TRANSLATE)
            
            for tr in TranslateFix.objects.all().annotate(l=Length('eng')).order_by('-l'):
                retr = re.compile(r'\b%s\b' % tr.eng, re.I+re.UNICODE)
                text = retr.sub(tr.rus, text)
            print ("To translate: [%s]" % text)
            text = translate.translate(text, 'en-ru')['text'][0]

            for btr in BackTranslate.objects.all():
                retr = re.compile(r'\b%s\b' % btr.rus, re.I+re.UNICODE)
                text = retr.sub(btr.eng, text)

            return text
        def translate_item(i):
            title_rus = my_translate(i.title)
            i.is_title_changed = False # reset attr
            i.is_subtitle_changed = False
            if i.sub_title:
                sub_title_rus = my_translate(i.sub_title)
                i.sub_title_rus = sub_title_rus
            if title_rus:
                i.title_rus = title_rus
                i.last_translate = timezone.now()
                #i.save(update_fields=['title_rus', "sub_title_rus"])
            print(i.url)
            print(("%s -> %s" % (i.title, title_rus)))
            if i.sub_title:
                print(("%s -> %s" % (i.sub_title, sub_title_rus)))
            i.save()
            
        for i in Item.objects.filter(is_active=True,
                                     is_title_changed=True).order_by("-last_translate")[:500]:
            translate_item(i)

        for i in Item.objects.filter(is_active=True).order_by("-last_translate")[:200]:
            translate_item(i)
            


        tr_models = ["ProductType", "Color", "Material", 
                     "PackageType", "ItemPropertyName", "ItemPropertyValue", 
                     "ItemPropertyType"]
        for tr_model in tr_models:
            print(("====== %s =====" % tr_model))
            for c in CL(tr_model).objects.filter(name_rus__isnull=True).exclude(name=''):
                rus = my_translate(c.name)
                if rus:
                    while CL(tr_model).objects.filter(name_rus=rus).count() > 0:
                        rus = "%s_" % rus
                    c.name_rus = rus
                    c.save(update_fields=['name_rus'])
                print(("%s -> %s" % (c.name, c.name_rus)))



            

