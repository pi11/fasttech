from django.core.management.base import BaseCommand, CommandError
from shop.models import *
from pyquery import PyQuery as pq
from djangohelpers.url import load_url
from shop.functions import get_exchange_rate

class Command(BaseCommand):
    help = 'Update exchange rate'

    def handle(self, *args, **options):
        option, c = Option.objects.get_or_create(name='exchange_rate')
        current_rate = get_exchange_rate()
        if settings.MIN_EXCHANGE_RATE < current_rate < settings.MAX_EXCHANGE_RATE:
            option.value = current_rate
            option.save()
