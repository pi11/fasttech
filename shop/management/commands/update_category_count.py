from django.core.management.base import BaseCommand, CommandError
from shop.models import Item, Category
from pyquery import PyQuery as pq
from djangohelpers.url import load_url

class Command(BaseCommand):
    help = 'Parse fasttech.com'

    def handle(self, *args, **options):
        for c in Category.objects.all():
            count = Item.objects.filter(category=c, stock__name="in_stock").count()
            c.count = count
            c.save(update_fields=['count'])

