import re, os
import sys
import requests
import json

from django.core.management.base import BaseCommand, CommandError
from django.urls import reverse

from shop.models import *
import vk

class Command(BaseCommand):
    help = 'Parse fasttech.com'

    def handle(self, *args, **options):
        item = Item.objects.filter(is_active=True, title_rus__isnull=False,
                                   is_vk_posted=False).order_by("-date_listed")[0]

        ex_url = "%s%s" % (settings.SITE_URL, reverse('redirect_item',
                                                   kwargs={"item_pk":item.pk}))
        print (ex_url)

        session = vk.Session(access_token=settings.VK_AT)
        api = vk.API(session)

        photos_ids = []
        no_images = False
        for ph in item.photos.all():
            if not ph.image:
                print ("Image not downloaded")
                no_images = True
                break
            if not os.path.exists(ph.image.path):
                print ("Image not exists")
                continue
            
            server = api.photos.getWallUploadServer(group_id=settings.VK_GROUP_ID)
            print (server)
            upload_url = server['upload_url']

            f = {'photo': open(ph.image.path, 'rb')}
            r = requests.post(upload_url, files=f)
            res = json.loads(r.text)
            print(res)

            photo = res['photo'].replace("\\","").replace("\\n", "\n").replace("\\r","\n")
            if photo == '[]':
                continue
            
            saved = api.photos.saveWallPhoto(photo=photo, hash=res['hash'],
                                             server=res['server'],
                                             group_id=settings.VK_GROUP_ID)
            photo_id =  saved[0]['id']
            photos_ids.append(photo_id)

        if no_images:
            pass
        else:
            atts = ",".join(photos_ids)
            print (atts)
            res = api.wall.post(owner_id="-%s" % settings.VK_GROUP_ID, from_group="1",
                                message="%s\n %s \n Цена: %s руб." % (item.title_rus, ex_url,
                                                           item.get_price()),
                                attachments=atts)
            print (res)

        item.is_vk_posted = True
        item.save()
        
