import cfscrape

from django.core.management.base import BaseCommand, CommandError
from shop.models import *
from pyquery import PyQuery as pq

class Command(BaseCommand):
    help = 'Parse fasttech.com'

    def handle(self, *args, **options):
        cats = ["https://www.fasttech.com/category/1421/battery-chargers-for-li-ion-ni-mh-ni-cd-lifepo4-an/-/p/%s?f=-",
                "https://www.fasttech.com/category/1420/batteries/-/p/%s?f=-",
                "https://www.fasttech.com/category/3099/e-cigarettes/-/p/%s?f=-",]
        j = 0
        scraper = cfscrape.CloudflareScraper()
        for root in cats:
            p = 1
            r = scraper.get(root % p)#, headers = {'referer': 'https://www.fasttech.com'})
            if r.status_code != 200:
                self.stdout.write(self.style.WARNING('Error loading page!'))
            else:
                data = r.content
                parsed = pq(data)
                last_page = parsed('a.PageLink:last').text()
            for p in range(1, int(last_page)):
                self.stdout.write(self.style.SUCCESS('Page: %s' % p))
                r = scraper.get(root % p)
                if r.status_code != 200:
                    self.stdout.write(self.style.WARNING('Error loading page!'))
                else:
                    data = r.content
                    parsed = pq(data)
                    parsed.make_links_absolute(base_url="https://www.fasttech.com")
                    items = parsed.items('.ProductGridItem')
                    for item in items:
                        url = item('.GridItemName a:first').attr('href')
                        if "products" in url:
                            self.stdout.write(url)
                            j += 1
                            utp, c = FasttechUrl.objects.get_or_create(url=url)

                    
        self.stdout.write(self.style.SUCCESS('Done, total %s urls added' % j))
