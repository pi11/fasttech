# -*- coding: utf-8 -*-

import time
import telepot
import traceback

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.urls import reverse

from shop.models import *


class Command(BaseCommand):
    args = ''
    help = 'Post new items to telegram channel'

    bot_help = u"""Telegram bot for posting news"""

    def handle(self, *args, **options):

        j = 0
        while 1:
            if settings.CHANNEL:
                j += 1
                if j == settings.CHANNEL_RATE:
                    j = 0
                    is_posted = False
                    items = Item.objects.filter(is_active=True,
                                                title_rus__isnull=False,
                                                is_telegram_posted=False)\
                                        .order_by("id")
                    while not is_posted:
                        for i in items:
                            try:
                                photo = item.photos.all()[0]
                            except IndexError:
                                print ("No images...")
                                continue
                            if not photo.image:
                                continue
                            try:
                                f = open(photo.image.path, 'rb')
                            except IOError:
                                print ("Broken file?...")
                                continue
                            response = bot.sendPhoto(settings.CHANNEL, f)
                            # also add text
                            url = reverse('redirect_item', item_pk=i.pk)
                            text = "%s - %s" % (i.title_rus, url)
                            i.is_telegram_posted = True
                            i.save(update_fields=['is_telegram_posted',])
                            is_posted = True
                            break
                        is_posted = True
            time.sleep(10)
