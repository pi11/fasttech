import sys
from django.core.management.base import BaseCommand, CommandError
from shop.models import *
from pyquery import PyQuery as pq
from djangohelpers.url import load_url

class Command(BaseCommand):
    help = 'Parse fasttech.com'

    def handle(self, *args, **options):
        j = 0
        total = Item.objects.all().count()
        for i in Item.objects.all():
            j += 1
            if j % 20 == 0:
                self.stdout.write(".", ending="")
                sys.stdout.flush()

            if j % 1000 == 0:
                print("%s of %s completed" % (j, total))
            i.save()

        self.stdout.write(self.style.SUCCESS('Done'))
