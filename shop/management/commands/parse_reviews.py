# parse fasttech pages
import time

from datetime import datetime
from django.core.management.base import BaseCommand, CommandError
from shop.models import *
from userapp.models import Review
from pyquery import PyQuery as pq
from djangohelpers.url import load_url
from django.utils import timezone

class Command(BaseCommand):
    help = 'Parse fasttech.com'

    def add_arguments(self, parser):
        parser.add_argument('pages_count', type=int)
        
    def handle(self, *args, **options):
        rus = "йцукенгшщзхфывапролджэячсмитьбю"
        for item in Item.objects.filter(is_active=True,
                                        reviews_parsed=False).order_by("id"):
            time.sleep(1)
            print ("Parsing: %s" % item.url.url)
            data = load_url(item.url.url, headers={"User-agent":"Mozilla/5.0 (X11; Linux x86_64; rv:6.0.1) Gecko/20110831 conkeror/0.9.3"})
            if not data:
                print ("Error loading page")
                print ("Sleep for 10 seconds")
                time.sleep(50)
                continue
                
            data = data.decode("utf-8")
            if not data:
                self.stdout.write(self.style.WARNING('Error loading page!'))
            else:
                parsed = pq(data)('table.Reviews tr').items()
                for p in parsed:
                    rc =  p('.ReviewContent')
                    is_rus = False
                    for r in rus:
                        if r in rc.text():
                            is_rus = True
                    if is_rus:
                        #print (rc.text())
                        nickname = (p('div.Nickname:first').text())
                        full_review = rc('a:first').attr('href')
                        if full_review:
                            print ("Loading full review >>> ", full_review)
                            review_data = load_url("https://www.fasttech.com%s" % full_review,
                                                   headers={"User-agent":"Mozilla/5.0 (X11; Linux x86_64; rv:6.0.1) Gecko/20110831 conkeror/0.9.3"})
                            review_data = review_data.decode("utf-8")
                            review_content = pq(review_data)('div.ReviewContent:first').text()
                        else:
                            review_content = rc.text()

                        print ("%s: [%s]" % (nickname, review_content))
                        review, c = Review.objects.get_or_create(item=item,
                                            nickname=nickname,
                                            text=review_content,
                                            is_moderated=True)

                item.reviews_parsed = True
                item.save(update_fields=['reviews_parsed',])
