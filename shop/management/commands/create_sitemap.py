import sys
from django.core.management.base import BaseCommand, CommandError
from django.core.urlresolvers import reverse
from django.conf import settings

from shop.models import *
from pyquery import PyQuery as pq
from djangohelpers.url import load_url

class Command(BaseCommand):
    help = 'Parse fasttech.com'

    def handle(self, *args, **options):
        j = 0
        q = Item.objects.filter(is_active=True, title_rus__isnull=False)
        total = q.count()
        sm_index = 1
        smap_file = "%s/sitemap%s.txt" % (settings.ABS_ROOT, sm_index)
        smap = open(smap_file, "w+")
        
        for i in q:
            url = "%s%s\n" % (settings.SITE_URL, reverse('item', kwargs={"item_pk":i.pk, "item_name":i.title}))
            url = url.replace("link//", "link/")
            smap.write(url)
            
            j += 1
            if j % 50 == 0:
                self.stdout.write(".", ending="")
                sys.stdout.flush()

            if j % 9000 == 0:
                print("\n%s of %s completed" % (j, total))
                sm_index += 1
                smap.close()
                smap_file = "%s/sitemap%s.txt" % (settings.ABS_ROOT, sm_index)
                print (smap_file)
                smap = open(smap_file, "w+")
                

        smap.close()
        self.stdout.write(self.style.SUCCESS('Done'))
