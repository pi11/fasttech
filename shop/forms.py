# -*- coding: utf-8 -*-
import hashlib
from datetime import date

from django.conf import settings
from django import forms
from django.forms import HiddenInput, Textarea
from django.forms import ModelForm, Form
from django.contrib.auth.models import User
from django.utils import timezone
from emailuser.models import EmailUserForm

from .models import *


class NewEmailUserForm(EmailUserForm):
    password = forms.CharField(max_length=100, required=False,
                               widget=forms.PasswordInput)


class PasswordForm(forms.Form):
    password1 = forms.CharField(max_length=100, required=True,
                                widget=forms.PasswordInput, label="Пароль")
    password2 = forms.CharField(max_length=100, required=True,
                                widget=forms.PasswordInput, label="Пароль еще раз")
    
    def clean_password2(self):
        data = self.cleaned_data['password2']
        if self.cleaned_data['password1'] != self.cleaned_data['password2']:
            raise forms.ValidationError("Пароли не совпадают")
        return data

    
class PaymentForm(forms.Form):
    """
    m 	ID Вашего магазина
    oa 	Сумма платежа
    o 	Номер заказа (также это может быть название товара или логин пользователя, для зачисления средств)
    s 	Подпись (методика формирования подписи в платежной форме)

    Необязательные параметры:
    i 	Предлагаемая валюта платежа (список валют). Плательщик сможет изменить ее в процессе оплаты.
    em 	Email плательщика
    lang 	Язык интерфейса оплаты (en/ru)
    us_key 	Так же Вы можете передавать свои параметры, которые наш сервер вернет на Ваш URL оповещения. Ключи параметров должны начинаться с us_ и содержать только латинские символы и цифры. Значения параметров могут содержать только латинские буквы, цифры и символы '-', '_'. 
    Например:

<input type="text" name="us_name" value="ivanov">
<input type="text" name="us_login" value="ivanov1971">

Внимание! Для передачи значений параметров на русском языке, необходимо передавать их в кодировке windows-1251
    """
    m = forms.CharField(widget=forms.HiddenInput())
    oa = forms.CharField(widget=forms.HiddenInput())
    o = forms.CharField(widget=forms.HiddenInput())
    s = forms.CharField(widget=forms.HiddenInput())

class AccountForm(forms.ModelForm):

    class Meta:
        model = RegisteredUser
        fields = ("first_name", "last_name", "patronymic", "phone",
                  "city", "zip", "address")


class SupportForm(forms.ModelForm):

    class Meta:
        model = SupportItem
        fields = ("question", )


class CouponForm(forms.Form):
    coupon_name = forms.CharField(label="")

    def clean_coupon_name(self):
        data = self.cleaned_data['coupon_name']
        try:
            coupon = Coupon.objects.get(name=data)
        except Coupon.DoesNotExist:
            raise forms.ValidationError("Купон не найден")
        if coupon.end_date < date.today():
            raise forms.ValidationError("Купон устарел")

        return data


class ReviewForm(forms.ModelForm):

    class Meta:
        model = Review
        fields = ("nickname", "text", )
    
