#-*- coding: utf-8 -*-
# fasttech shop crawler

import math

from django.db import models
from django.conf import settings
from djangohelpers.utils import wilson_score
from djangohelpers.images import scale


class ParseStat(models.Model):
    new_titles = models.IntegerField(default=0, verbose_name="Тайтлов изменилось")
    new_items = models.IntegerField(default=0, verbose_name="Добавлено товаров")
    parse_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ["-parse_date", ]
        verbose_name = "Статистика парсинга"
    
class Option(models.Model):
    name = models.CharField(max_length=100, unique=True)
    value = models.CharField(max_length=100)
    
    class Meta:
        ordering = ["name", ]
        verbose_name = "Опция"
        verbose_name_plural = "Опции"

    def __str__(self):
        return self.name


class TranslateFix(models.Model):
    eng = models.CharField(max_length=100, unique=True,
                           help_text="Английское слово")
    rus = models.CharField(max_length=100,
                           help_text="Перевод")

    class Meta:
        ordering = ["eng", "rus"]
        verbose_name = "Исправление перевода"
        verbose_name_plural = "Исправления переводов"

class BackTranslate(models.Model):
    rus = models.CharField(max_length=100, unique=True)
    eng = models.CharField(max_length=100)

    class Meta:
        ordering = ["eng", "rus"]
        verbose_name = "Обратное исправление перевода"
        verbose_name_plural = "Обратные исправления переводов"

    
class ActiveManager(models.Manager):
     def get_queryset(self):
        return super(ActiveManager, self).get_queryset().filter(is_active=True)

class CategoryManager(models.Manager):
     def get_queryset(self):
        return super(CategoryManager, self).get_queryset()

class Category(models.Model):
    name = models.CharField(max_length=150, unique=True, 
                            verbose_name="Английское название",
                            help_text="это поле не изменять!")
    name_rus = models.CharField(max_length=150, unique=True,
                                 null=True, blank=True, verbose_name="Русское название")
    is_translated = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False, help_text="Отображать категорию")
    order = models.IntegerField(default=0, help_text="Порядковый номер в меню")
    count = models.IntegerField(default=0, editable=False)

    home_page = models.BooleanField(default=False)
    home_page_name = models.CharField(null=True, blank=True, max_length=150)
    
    active_objects = ActiveManager()
    objects = CategoryManager()

    use_translate = models.BooleanField(default=True)
    
    class Meta:
        ordering = ["-order", "name_rus"]
        verbose_name = "Раздел"
        verbose_name_plural = "Разделы"

    def __str__(self):
        if self.name_rus:
            return self.name_rus
        else:
            return self.name


class SearchStat(models.Model):
    query = models.CharField(unique=True, max_length=200)
    count = models.IntegerField(default=1)

    def __str__(self):
        return self.query

class SearchStatCategory(models.Model):
    query = models.ForeignKey(SearchStat, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    count = models.IntegerField(default=0)
    last_update = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.query)

        
class TopCategory(models.Model):
    name_rus = models.CharField(max_length=100, unique=True, verbose_name="Название")
    categories = models.ManyToManyField(Category, verbose_name="Категории")
    order = models.IntegerField(default=0, help_text="Порядковый номер в меню")

    count = models.IntegerField(default=0, editable=False)

    class Meta:
        ordering = ["-order", "name_rus"]
        verbose_name = "Категория"
        verbose_name_plural = "Категории"

    def __str__(self):
        return self.name_rus

    def active_categories(self):
        return self.categories.filter(is_active=True, count__gt=0)

class ProductType(models.Model):
    name = models.CharField(max_length=150, unique=True)
    name_rus = models.CharField(max_length=150, unique=True,
                                null=True, blank=True)
    description = models.CharField(max_length=512, null=True, blank=True)
    description_rus = models.CharField(max_length=512, null=True, blank=True)
    is_translated = models.BooleanField(default=False)
    count = models.IntegerField(default=0)

    class Meta:
        ordering = ["name_rus"]
        verbose_name = "Тип продукта"
        verbose_name_plural = "Типы продуктов"
    
    def __str__(self):
        if self.name_rus:
            if self.name_rus != '':
                return self.name_rus
        return self.name
    
class Color(models.Model):
    name = models.CharField(max_length=150, unique=True)
    name_rus = models.CharField(max_length=150, unique=True,
                                null=True, blank=True)
    is_translated = models.BooleanField(default=False)
    count = models.IntegerField(default=0)

    class Meta:
        ordering = ["name_rus"]
        verbose_name = "Цвет"
        verbose_name_plural = "Цвета"

    def __str__(self):
        if self.name_rus:
            if self.name_rus != '':
                return self.name_rus
        return self.name


class Material(models.Model):
    name = models.CharField(max_length=150, unique=True)
    name_rus = models.CharField(max_length=150, unique=True,
                                null=True, blank=True)
    is_translated = models.BooleanField(default=False)
    count = models.IntegerField(default=0)

    class Meta:
        ordering = ["name_rus"]
        verbose_name = "Материал"
        verbose_name_plural = "Материалы"

    def __str__(self):
        if self.name_rus:
            if self.name_rus != '':
                return self.name_rus
        return self.name

    
class PackageType(models.Model):
    name = models.CharField(max_length=250, unique=True)
    name_rus = models.CharField(max_length=250, unique=True,
                                null=True, blank=True)
    description = models.CharField(max_length=1000, null=True, blank=True)
    description_rus = models.CharField(max_length=1000, null=True, blank=True)
    is_translated = models.BooleanField(default=False)
    count = models.IntegerField(default=0)

    class Meta:
        ordering = ["name_rus"]
        verbose_name = "Тип упаковки"
        verbose_name_plural = "Типы упаковки"

    def __str__(self):
        return self.name


    
class ItemPropertyType(models.Model):
    name = models.CharField(max_length=150, unique=True)
    name_rus = models.CharField(max_length=100, unique=True,
                                         null=True, blank=True)

    class Meta:
        ordering = ["name_rus"]
        verbose_name = "Тип свойства"
        verbose_name_plural = "Типы свойства"

    def __str__(self):
        if self.name_rus:
            return self.name_rus
        return self.name


class ItemPropertyName(models.Model):
    name = models.CharField(max_length=150, unique=True)
    name_rus = models.CharField(max_length=100, unique=True,
                                         null=True, blank=True)
    
    class Meta:
        ordering = ["name_rus"]
        verbose_name = "Название свойства"
        verbose_name_plural = "Название свойства"

    def __str__(self):
        if self.name_rus:
            return self.name_rus
        return self.name


class ItemPropertyValue(models.Model):
    name = models.CharField(max_length=380, unique=True)
    name_rus = models.CharField(max_length=380, unique=True,
                                         null=True, blank=True)

    class Meta:
        ordering = ["name_rus"]
        verbose_name = "Значение свойства"
        verbose_name_plural = "Значения свойств"

    def __str__(self):
        if self.name_rus:
            return self.name_rus
        return self.name
    

    
class ItemProperty(models.Model):
    property_type = models.ForeignKey(ItemPropertyType, on_delete=models.CASCADE)

    property_name = models.ForeignKey(ItemPropertyName, on_delete=models.CASCADE)
    property_value = models.ForeignKey(ItemPropertyValue,
                                       on_delete=models.CASCADE)
    
    class Meta:
        verbose_name = "Свойство"
        verbose_name_plural = "Свойства"

    def __str__(self):
        return "%s: %s" % (self.property_name, self.property_value)
    

class Photo(models.Model):
    url = models.CharField(max_length=512, unique=True)
    image = models.ImageField(null=True, blank=True, upload_to="m/%y%m%d/")

    is_main = models.BooleanField(default=False)
    
    class Meta:
        verbose_name = "Фото"
        verbose_name_plural = "Фото"
        ordering = ("-is_main", "id")
        
    def __str__(self):
        return self.url

    def get_url(self):
        if self.image:
            return self.image.url.replace(settings.MEDIA_ROOT, '')
        else:
            return self.url

    def get_thumb(self):
        if self.image:
            try:
                p = scale(self.image, settings.THUMB_SIZE).replace(settings.MEDIA_ROOT, '')
            except (OSError, ValueError):
                #self.delete() #image = False
                #self.save(update_fields=['image',])
                p = ""
            return p
        else:
            return self.url

class FasttechUrl(models.Model):
    url = models.CharField(max_length=250, unique=True)
    is_parsed = models.BooleanField(default=False)
    parsed_at = models.DateTimeField(null=True, blank=True)
    is_removed = models.BooleanField(default=False)
    added = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ["-parsed_at", ]
        verbose_name = "Урл на фастеке"
        verbose_name_plural = "Урлы на фастеке"

    def __str__(self):
        return self.url
    

class Stock(models.Model):
    name = models.CharField(max_length=50, unique=True)
    avail = models.IntegerField(default=1) # 1 = in stock, 0 - no
    
    class Meta:
        verbose_name = "Словарь наличия"
        verbose_name_plural = "Словарь наличия"

    def __str__(self):
        return self.name
    
    
class ItemDescription(models.Model):
    item = models.OneToOneField('Item', on_delete=models.CASCADE)
    description = models.TextField()
    is_description_changed = models.BooleanField(default=False)
    
    class Meta:
        verbose_name = "Описание товара"
        verbose_name_plural = "Описания товаров"
    
class Item(models.Model):

    sku = models.CharField(max_length=20, unique=True, null=True, blank=True,
                           verbose_name="Номер товара на Фастеке")
    date_listed = models.DateField(null=True, blank=True,
                                   verbose_name="Дата публикации на фастеке")
    
    title = models.CharField(max_length=500, verbose_name="Название")
    is_title_changed = models.BooleanField(default=False)
    title_rus = models.CharField(max_length=250, null=True, blank=True,
                                 verbose_name="Название русское")

    sub_title = models.CharField(max_length=500, null=True, blank=True,
                                 verbose_name="Доп. описание")
    is_subtitle_changed = models.BooleanField(default=False)
    sub_title_rus = models.CharField(max_length=250, null=True, blank=True,
                                     verbose_name="Доп. описание русское")
    
    rate = models.FloatField(default=0, verbose_name="Рейтинг")
    rate_count = models.IntegerField(default=0)
    
    item_properties = models.ManyToManyField(ItemProperty, editable=False,
                                             verbose_name="Свойства")
    
    color = models.ForeignKey(Color, null=True, blank=True, on_delete=models.PROTECT)
    material = models.ForeignKey(Material, null=True, blank=True,
                                 on_delete=models.PROTECT)
    product_type = models.ForeignKey(ProductType, null=True, blank=True,
                                     on_delete=models.PROTECT)
    package_type = models.ForeignKey(PackageType, null=True, blank=True,
                                     on_delete=models.PROTECT)

    price = models.FloatField(default=0)
    stock = models.ForeignKey(Stock, on_delete=models.PROTECT)
    stock_last_check = models.DateTimeField(auto_now_add=True)
    stock_days = models.IntegerField(default=0) #
    photos = models.ManyToManyField(Photo, editable=False)
    url = models.ForeignKey(FasttechUrl, editable=False, null=True, blank=True,
                            on_delete=models.PROTECT)
    category = models.ForeignKey(Category, null=True, blank=True,
                                 on_delete=models.PROTECT)
    is_active = models.BooleanField(default=True)

    views = models.IntegerField(default=0, editable=False)
    purchases = models.IntegerField(default=0, editable=False)

    total_votes = models.IntegerField(default=0)
    rate = models.FloatField(default=0)

    wilson_score = models.FloatField(default=0)


    home_page = models.BooleanField(default=False, verbose_name="Отображать на главной странице")

    last_translate = models.DateTimeField(auto_now_add=True)
    added = models.DateTimeField(auto_now_add=True)
    reviews_parsed = models.BooleanField(default=False)
                                         
    is_vk_posted = models.BooleanField(default=False)
    is_telegram_posted = models.BooleanField(default=False)

    is_video_review_parsed = models.BooleanField(default=False)
    
    class Meta:
        verbose_name = "Товар"
        verbose_name_plural = "Товары"

    def __str__(self):
        return "%s: %s" %  (self.title, self.sku)

    def save(self, *args, **kwargs):
        if self.total_votes > 0:
            self.wilson_score = wilson_score(self.rate * self.total_votes, 
                                   self.total_votes, [0,1,2,3,4,5])

        v = super(Item, self).save(*args, **kwargs)
        return v


    def estimate_send_date(self):
        return self.stock_days
    
    def get_rate_width(self):
        """Return width of div element represent rate"""
        if self.rate == 0:
            return 0
        return int(18.5 * self.rate)
        
    def get_url(self):
        return "<a href='%s' target='_blank'>%s</a>" % (self.url.url, self.url.url)
    get_url.allow_tags = True

    def get_photo(self):
        try:
            return self.photos.all()[0].get_url()
        except IndexError:
            try:
                return self.photos.filter(is_main=True)[0].get_url()
            except IndexError:
                return False
        return False

    def get_thumb(self):
        try:
            return self.photos.all()[0].get_thumb()
        except IndexError:
            return False
        
    def get_thumb_admin(self):
        return "<img src='%s' width='200px' />" % self.get_photo()
    get_thumb_admin.allow_tags = True

    def get_price(self):
        try:
            course = Option.objects.get(name='exchange_rate')
        except Option.DoesNotExist:
            course = settings.EXCHANGE_RATE
        else:
            try:
                course = float(course.value)
            except ValueError:
                course = settings.EXCHANGE_RATE
        
        try:
            profit = Option.objects.get(name='profit')
        except Option.DoesNotExist:
            profit = settings.PROFIT
        else:
            try:
                profit = float(profit.value)
            except ValueError:
                profit = settings.PROFIT
        if self.price < 1.5: # if proce too low profit == 2
            profit = 2
        return int(math.ceil(course * self.price * profit))

    def get_title(self):
        if self.category.use_translate:
            if self.title_rus:
                return self.title_rus
        return self.title
        

class ItemRelatedSKU(models.Model):
    item = models.ForeignKey(Item, on_delete=models.PROTECT)
    related_item = models.ForeignKey(Item, null=True, blank=True,
                                     related_name="related_item",
                                     on_delete=models.PROTECT)
    related_sku = models.CharField(max_length=50)

    def __str__(self):
        return self.item.title
