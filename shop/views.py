import traceback
from datetime import datetime, timedelta

from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string
from django.http import (HttpResponseNotFound, HttpResponseRedirect, Http404)
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.postgres.search import SearchQuery, SearchRank, SearchVector
from django.urls import reverse
from django.conf import settings

from sphinxql.query import SearchQuerySet
from .models import Item, Category, ItemRelatedSKU, Photo, SearchStat, SearchStatCategory
from userapp.models import Review, VideoReview, Shipment, PostTracker
from userapp.forms import ReviewForm
from knowledge.forms import QuestionItemForm
from .functions import prepare_page

from .indexes import ItemIndex


def get_home_items():
    items = []
    for c in Category.objects.filter(home_page=True).order_by("order"):
        if c.home_page_name:
            name = c.home_page_name
        else:
            name = c.name_rus
        q_items = [item for item in Item.objects.filter(
            title_rus__isnull=False, is_active=True, category=c,
            stock__avail=1).order_by("-wilson_score")[:3]]
        q_items2 = [item for item in Item.objects.filter(
            title_rus__isnull=False, stock__avail=1, is_active=True, category=c
            ).order_by("-pk")[:3]]
        q_items3 = [item for item in Item.objects.filter(
            title_rus__isnull=False, is_active=True, category=c, home_page=True,
            stock__avail=1).order_by("-pk")[:5]]
        
        items.append({"cat_name":c.name, "name":name,
                      "q":set(q_items+q_items2+q_items3)})
    return items


def home(request):
    parcels = Shipment.objects.filter().order_by("-added")[:15]
    posttracks = PostTracker.objects.filter(delivery_date__isnull=False)
    total, i = 0, 0
    for p in posttracks:
        if p.shipment.get_total_time() != '':
            total += p.shipment.get_total_time()
            i += 1
    if total > 0:
        avg_time = total // i
    else:
        avg_time = 0
    return render(request, "home.html", {"home_items":get_home_items(),
                                         "parcels":parcels,
                                         "avg_time":avg_time})


def new(request):
    items = Item.objects.filter(title_rus__isnull=False, is_active=True,
                                date_listed__isnull=False).order_by("-date_listed")
    page, page_range = prepare_page(request, items)
    return render(request, "new.html", {"page": page, 
                                        "page_range": page_range,})


def category(request, cat_name):
    sort = request.GET.get('s', False)
    if not sort:
        real_sort = "-wilson_score"
    else:
        real_sort = sort
    sorts = [{"sort": "date_listed", "bsort": "-date_listed", "title": "дате"},
             {"sort": "price", "bsort": "-price", "title": "цене"},
             {"sort": "-wilson_score", "bsort": "wilson_score", "title": "рейтингу"},]

    if real_sort not in ['date_listed', 'wilson_score', 'price'] and \
      real_sort not in ['-date_listed', '-wilson_score', '-price']:
        raise Http404
    c = get_object_or_404(Category, name=cat_name)
    items = Item.objects.filter(
        category=c, title_rus__isnull=False,
        stock__avail=True).order_by(real_sort)
    
    page, page_range = prepare_page(request, items)
    return render(request, "category.html", locals())


def search(request):
    q = request.GET.get("q", "")
    if q == "":
        pass
    else:
        s_stat, c = SearchStat.objects.get_or_create(query=q.lower())
        if not c:
            s_stat.count += 1
            s_stat.save()
        categories = Category.objects.filter(is_active=True)
        
        order = request.GET.get('order', "-rank")
        cat = request.GET.get('cat', False)
        res = SearchQuerySet(ItemIndex).search(q)
        if cat:
            category = Category.objects.get(pk=int(cat))
            if "rank" in order:
                items = SearchQuerySet(ItemIndex).search(q)\
                                                 .filter(category=category)
            else:
                items = SearchQuerySet(ItemIndex).search(q).\
                        filter(category=category).order_by(order)
        else:
            if "rank" in order:
                items = SearchQuerySet(ItemIndex).search(q)
            else:
                items = SearchQuerySet(ItemIndex).search(q).order_by(order)

        items = items.filter(is_active=True, stock__avail=True)
        c_categories = []

        dt = datetime.now() - timedelta(days=1)
        stcats = SearchStatCategory.objects.filter(query=s_stat, last_update__gt=dt) # check if we have populated search stat
        if len(stcats) > 0:
            for c in stcats:
                c_categories.append([c.category, c.count])

        # if parse stat per category if outdated or empty
        # lets populated it
        if len(c_categories) == 0:
            SearchStatCategory.objects.filter(query=s_stat).delete()
            for c in categories:
                count = len(SearchQuerySet(ItemIndex).search(q).filter(category=c))
                if count > 0:
                    c_categories.append([c, count])
                    stc, cr = SearchStatCategory.objects.get_or_create(query=s_stat, category=c)
                    stc.count = count
                    stc.last_update = datetime.now()
                    stc.save()
                
        page, page_range = prepare_page(request, items)
    return render(request, "search.html", locals())


def item(request, item_name, item_pk):
    added = request.GET.get("added", False)
    item = get_object_or_404(Item, title=item_name, pk=item_pk)
    item.views += 1
    item.save(update_fields=['views'])
    related = ItemRelatedSKU.objects.filter(item=item, related_item__isnull=False)
    reviews = Review.objects.filter(item=item, is_active=True, is_moderated=True)
    video_reviews = VideoReview.objects.filter(item=item, is_active=True, is_moderated=True)
    
    if request.user.is_authenticated:
        user = request.user
        initial = {"nickname":user.email.split("@")[0]}
    else:
        initial = {"nickname":"Anonymous"}
    form = ReviewForm(initial=initial)
    question_item_form = QuestionItemForm(initial={"item":item})
    return render(request, "item.html",
                  {"item": item, "user": request.user, "related": related,
                   "reviews": reviews, "form": form, "added": added,
                   "video_reviews": video_reviews,
                   "question_item_form": question_item_form,
                   "SITE_URL_NO_SLASH": settings.SITE_URL_NO_SLASH})


def page404(request, exception):
    #c = RequestContext(request)
    try:
        res = render_to_string('404-main.html', 
                               {"home_items": get_home_items()}, request)
    except:
        print(traceback.format_exc())
    return HttpResponseNotFound(res)


@staff_member_required
def set_main_photo(request, photo_pk, item_pk):
    ret_page = request.GET.get('next', '')
    ph = get_object_or_404(Photo, pk=photo_pk)
    item = get_object_or_404(Item, pk=item_pk)
    for photo in item.photos.all():
        photo.is_main = False
        photo.save(update_fields=['is_main',])
    ph.is_main = True
    ph.save(update_fields=['is_main',])
    return HttpResponseRedirect(ret_page)


def redirect_item(request, item_pk):
    item = get_object_or_404(Item, pk=item_pk)
    return HttpResponseRedirect(reverse('item', kwargs={"item_name":item.title, "item_pk":item.pk}))

