from sphinxql import indexes, fields
from shop import models

class ItemIndex(indexes.Index):
    sku = fields.Text(model_attr='sku')
    title = fields.Text(model_attr='title')
    title_rus = fields.Text(model_attr='title_rus')
    sub_title = fields.Text(model_attr='sub_title')
    sub_title_rus = fields.Text(model_attr='sub_title_rus')

    category = fields.Integer(model_attr='category')

    class Meta:
        model = models.Item
