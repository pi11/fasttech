# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-09-09 00:41
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0017_item_is_active'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='stock_last_check',
            field=models.DateTimeField(auto_now_add=True, default=datetime.datetime(2014, 9, 9, 0, 41, 15, 695493, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
