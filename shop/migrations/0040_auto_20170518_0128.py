# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-18 01:28
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0039_item_added'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='home_page',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='category',
            name='home_page_name',
            field=models.CharField(blank=True, max_length=150, null=True),
        ),
    ]
