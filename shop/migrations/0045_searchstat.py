# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-07-21 03:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0044_auto_20170721_0303'),
    ]

    operations = [
        migrations.CreateModel(
            name='SearchStat',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('query', models.CharField(max_length=200, unique=True)),
                ('count', models.IntegerField(default=1)),
            ],
        ),
    ]
