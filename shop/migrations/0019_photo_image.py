# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-09-09 11:04
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0018_item_stock_last_check'),
    ]

    operations = [
        migrations.AddField(
            model_name='photo',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to='m/%Y%m%d/'),
        ),
    ]
