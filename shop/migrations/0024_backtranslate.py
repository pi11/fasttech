# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-09-10 01:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0023_translatefix'),
    ]

    operations = [
        migrations.CreateModel(
            name='BackTranslate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('rus', models.CharField(max_length=100, unique=True)),
                ('eng', models.CharField(max_length=100, unique=True)),
            ],
        ),
    ]
