# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-11 10:54
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0003_auto_20160710_1709'),
    ]

    operations = [
        migrations.RenameField(
            model_name='category',
            old_name='title',
            new_name='name',
        ),
        migrations.RenameField(
            model_name='category',
            old_name='title_rus',
            new_name='name_rus',
        ),
    ]
