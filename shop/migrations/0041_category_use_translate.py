# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-12 23:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0040_auto_20170518_0128'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='use_translate',
            field=models.BooleanField(default=True),
        ),
    ]
