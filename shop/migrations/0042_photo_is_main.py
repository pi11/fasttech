# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-12 23:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0041_category_use_translate'),
    ]

    operations = [
        migrations.AddField(
            model_name='photo',
            name='is_main',
            field=models.BooleanField(default=False),
        ),
    ]
