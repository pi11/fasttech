# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-18 23:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0042_photo_is_main'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='photo',
            options={'ordering': ('-is_main', 'id'), 'verbose_name': 'Фото', 'verbose_name_plural': 'Фото'},
        ),
        migrations.AddField(
            model_name='item',
            name='reviews_parsed',
            field=models.BooleanField(default=False),
        ),
    ]
