# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-19 18:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0011_auto_20160719_1544'),
    ]

    operations = [
        migrations.AlterField(
            model_name='itempropertyname',
            name='name',
            field=models.CharField(max_length=150, unique=True),
        ),
        migrations.AlterField(
            model_name='itempropertytype',
            name='name',
            field=models.CharField(max_length=150, unique=True),
        ),
        migrations.AlterField(
            model_name='packagetype',
            name='name',
            field=models.CharField(max_length=250, unique=True),
        ),
        migrations.AlterField(
            model_name='packagetype',
            name='name_rus',
            field=models.CharField(blank=True, max_length=250, null=True, unique=True),
        ),
    ]
