# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-10 17:09
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0002_auto_20160710_1707'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='UrlToParse',
            new_name='FasttechUrl',
        ),
        migrations.AddField(
            model_name='item',
            name='url',
            field=models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, to='shop.FasttechUrl'),
            preserve_default=False,
        ),
    ]
