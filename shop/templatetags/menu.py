from django.template import Library, Node
from django.conf import settings

from shop.models import Category, TopCategory
from cms.models import Page

register = Library()

def build_categories_list(parser, token):
    """
    {% get_category_list %}
    """
    return CategoryMenuObject()

class CategoryMenuObject(Node):
    def render(self, context):
        categories_list = TopCategory.objects.filter().order_by("order", "name_rus")
        context['categories_list'] = categories_list
        return ''

register.tag('get_categories_list', build_categories_list)


def build_menu(parser, token):
    """
    {% get_menu %}
    """
    return MenuObject()

class MenuObject(Node):
    def render(self, context):
        menu = Page.objects.filter(is_active=True).order_by("order")
        context['menu'] = menu
        return ''

register.tag('get_menu', build_menu)

