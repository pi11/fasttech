from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^$', home, name="home"),
    url(r'^new/$', new, name="new"),
    url(r'^category/(?P<cat_name>.*?)/$', category, name="category"),
    url(r'^search/$', search, name="search"),
    url(r'^item/(?P<item_name>.*?)/(?P<item_pk>\d+)/$', item, name="item"),

    url(r'^staff/set-main-photo/(?P<photo_pk>\d+)/(?P<item_pk>\d+)/$', set_main_photo, name="set_main_photo"),
    url('i/(?P<item_pk>\d+)', redirect_item, name="redirect_item"),

]
