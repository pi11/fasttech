#-*- coding: utf-8 -*-

import xml.dom.minidom

from django.conf import settings
from django.http import Http404
from django.core.paginator import Paginator, InvalidPage, EmptyPage

from djangohelpers.utils import get_page_range
from djangohelpers.url import load_url

def get_exchange_rate(currency_code=840):
    """Return float exchange rate for currency"""
    def parse_xml(data):
        xml_nodes = {}
        xml_list = []
        xmldoc = xml.dom.minidom.parseString(data)
        root = xmldoc.documentElement
        for valute in root.childNodes:
            if valute.nodeName == '#text':
                continue
            for ch in valute.childNodes:
                if ch.nodeName == '#text': # Drop TextNode, that is means "\n" in the xml document
                    continue
                xml_nodes[ch.nodeName] = ch.childNodes[0].nodeValue
            xml_list.append(xml_nodes)
            xml_nodes = {}
        return xml_list

    xml_nodes = parse_xml(load_url('http://www.cbr.ru/scripts/XML_daily.asp'))
    for node in xml_nodes:
        if int(node['NumCode']) == currency_code:
            return float(node['Value'].replace(",", "."))


def prepare_page(request, query, objects_per_page=20, page_name='p'):
    """Prepare pagination
    return paginator and page_range"""
    try:
        page_id = int(request.GET.get(page_name, 1))
    except ValueError:
        raise Http404
    paginator = Paginator(query, objects_per_page)
    if page_id > paginator.num_pages:
        page_id = paginator.num_pages

    try:
        page = paginator.page(page_id)
    except EmptyPage:
        page = False
    page_range = get_page_range(paginator, page_id)
    return page, page_range


