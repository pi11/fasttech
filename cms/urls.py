from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'page/(?P<url>.*?)/$', page, name='page'),
]
