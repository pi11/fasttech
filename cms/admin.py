#-*- coding: utf-8 -*-

from django.contrib import admin

from .models import *

class PageAdmin(admin.ModelAdmin):
    pass

admin.site.register(Page, PageAdmin)

