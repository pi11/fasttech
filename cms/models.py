#-*- coding: utf-8 -*-



from django.db import models

class Page(models.Model):
    url = models.CharField(max_length=100, unique=True)
    title = models.CharField(max_length=250)
    link_title = models.CharField(max_length=50)
    content = models.TextField()
    home_page = models.BooleanField(default=False, help_text="Отображать на главной странице")
    top_menu = models.BooleanField(default=False, help_text="Отображать наверху страницы")
    is_active = models.BooleanField(default=True, help_text="Активна ли страница")

    icon = models.ImageField(upload_to="icons/", null=True, blank=True)
    order = models.IntegerField(default=0, help_text="Порядок отображения")

    def __str__(self):
        return self.link_title

