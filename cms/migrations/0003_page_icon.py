# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-09-13 01:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0002_auto_20160909_2205'),
    ]

    operations = [
        migrations.AddField(
            model_name='page',
            name='icon',
            field=models.ImageField(blank=True, null=True, upload_to='media/'),
        ),
    ]
