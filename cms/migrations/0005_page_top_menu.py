# Generated by Django 2.0b1 on 2017-11-12 01:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0004_auto_20160916_2051'),
    ]

    operations = [
        migrations.AddField(
            model_name='page',
            name='top_menu',
            field=models.BooleanField(default=False, help_text='Отображать наверху страницы'),
        ),
    ]
