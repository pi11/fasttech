from django.conf.urls import url, include
from django.contrib import admin

from shop.views import page404
handler404 = page404

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('shop.urls')),
    url(r'^api/', include('api.urls')),
    url(r'^pages/', include('cms.urls')),
    url(r'^user/', include('userapp.urls')),
    url(r'^manager/', include('manager.urls', namespace="manager")),
    url(r'^chat/', include('consult.urls', namespace="consult")),
    url(r'^help/', include('knowledge.urls', namespace="knowledge")),
]
