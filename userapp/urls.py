from django.conf.urls import include, url

from userapp.views import *

urlpatterns = [
    url(r'^login/$', login_view, name='login'),
    url(r'^logout/$', logout_view, name='logout'),
    url(r'^auth/(?P<user_id>\d+)/(?P<token>\w+)/$', auth, name='auth'),

    url(r'^account/$', account, name='account'),
    url(r'^account/orders/history$', orders_history, name='orders_history'),
    url(r'^account/order/(?P<order_pk>\d+)/$', order_info, name='order_info'),
    url(r'^account/shipment/(?P<shipment_pk>\d+)/$', shipment_info, name='shipment_info'),

    url(r'^account/edit-ship-address/$', edit_ship_address, name='edit_ship_address'),
    url(r'^account/set-password/$', set_password, name='set_password'),


    url(r'^basket/$', basket, name='basket'),
    url(r'^basket/amount/$', basket_amount, name='basket_amount'),
    url(r'^basket/amount/change$', basket_amount_change, name='basket_amount_change'),
    url(r'^basket/strong-package$', basket_strong_package, name='basket_strong_package'),
    url(r'^basket/add-item/(?P<item_id>\d+)/$', add_item, name='add_item'),
    url(r'^basket/remove-item/(?P<item_id>\d+)/$', remove_item, name='remove_item'),
    url(r'^basket/order-add-comment/(?P<order_id>\d+)/$', order_add_comment, name='order_add_comment'),
    url(r'^review/add/(?P<item_pk>\d+)/$', add_review, name='add_review'),


]
