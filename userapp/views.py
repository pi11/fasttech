# -*- coding: utf-8 -*-
""" userapp views"""

from datetime import datetime
import hashlib
import json
import traceback
# import re

from django.contrib.auth import authenticate, login
from django.contrib.auth import logout
from django.conf import settings
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.core.paginator import InvalidPage, EmptyPage
from django.shortcuts import render
from django.urls import reverse

from djangohelpers.utils import gen_url
from emailuser.models import  manual_create_email_user
from .models import *
from .forms import AccountForm, PaymentForm, CouponForm, \
    ReviewForm, NewEmailUserForm, PasswordForm, OrderCommentForm
from shop.models import Item
from emailservice.models import EmailObject


def login_view(request):
    next_url = request.GET.get('next', '')
    sended = False
    if request.method == 'POST':
        form = NewEmailUserForm(request.POST)
        if form.is_valid():  # All validation rules pass
            email = form.cleaned_data['email']
            try:
                user = User.objects.get(email=email)
            except User.DoesNotExist:
                user = User.objects.create_user(
                    email=form.cleaned_data['email'],
                    username=form.cleaned_data[
                        'email'],
                    password=gen_url(20))
                user.save()
            else:
                if form.cleaned_data['password']:
                    check_user = authenticate(username=user.username,
                                        password=form.cleaned_data['password'])
                    if check_user is not None:
                        if check_user.is_active:
                            login(request, check_user)
                            return render(request, "user/login.html",
                                          {'form': form, 'sended': False,
                                           'authenticated':True})
            try:
                token = user.emailuser.generate_token()
            except:
                manual_create_email_user(user)
                token = user.emailuser.generate_token()
            # try:
            if settings.DEBUG:
                print("Token - %s" % token)
            message = ('\nДля входа на сайт пройдите по ссылке:\n'
                       '%s/user/auth/%s/%s/?next=%s \n'
                       'Если у Вас возникли проблемы со входом '
                       'напишите письмо на %s' % (settings.SITE_URL, user.pk,
                                                  token, next_url,
                                                  settings.SERVER_EMAIL))
            
            eo, c = EmailObject.objects.get_or_create(
                subject='Вход на сайт %s' % settings.SITE_URL, 
                text=message, to=email, priority=1, retries=0, is_sended=False)
            #if not c:
                
            sended = True
    else:
        form = NewEmailUserForm()
    return render(request, "user/login.html", {'form': form, 'sended': sended})


def auth(request, user_id, token):
    next_url = request.GET.get('next', None)
    get_user = get_object_or_404(User, pk=user_id)
    user = authenticate(email=get_user.email, token=token)
    if user is not None:
        if user.is_active:
            login(request, user)
    return render(request, "user/auth.html", {"user": user, "next":next_url})


def logout_view(request):
    logout(request)
    return redirect('home')


@login_required
def account(request):
    s_user = SUser(request)
    user = request.user
    email_user = user.emailuser
    orders = Order.objects.filter(user=email_user, is_deleted=False,
                                  is_payed=True).count()
    registered_user, c = RegisteredUser.objects.get_or_create(user=email_user)

    return render(request, 'user/account.html', locals())


@login_required
def edit_ship_address(request):
    s_user = SUser(request)
    user = request.user
    email_user = user.emailuser
    registered_user, c = RegisteredUser.objects.get_or_create(user=email_user)
    if registered_user.s_user != s_user.get_user:
        registered_user.s_user = s_user.get_user
        registered_user.save(update_fields=['s_user'])
    saved = False
    if request.method == 'POST':
        form = AccountForm(request.POST, instance=registered_user)
        if form.is_valid():
            address = form.save()
            next_url = request.GET.get('next', False)
            if next_url:
                return redirect(next_url)
            #return redirect('edit_ship_address')
            saved = True
    else:
        form = AccountForm(instance=registered_user)
    
    return render(request, 'user/edit-ship-address.html', 
                  {"form": form, "ru": registered_user})
    

def add_item(request, item_id):
    """ Add Item to basket """

    item = get_object_or_404(Item, pk=item_id)
    result = {}
    s_user = SUser(request)
    basket, c = Basket.objects.get_or_create(user=s_user.get_user)
    basket_item, c = BasketItem.objects.get_or_create(basket=basket, item=item)
    #if not c:
    #    basket.amount += 1
    #    basket.save(update_fields=["amount", ])
    result['total'] = BasketItem.objects.filter(basket=basket).count()
    result['item'] = item.pk
    js = json.dumps(result)
    return HttpResponse(js, content_type='application/json')


def remove_item(request, item_id):
    """ remove Item from basket """

    s_user = SUser(request)
    item = get_object_or_404(Item, pk=item_id)
    basket = get_object_or_404(Basket, user=s_user.get_user)
    BasketItem.objects.filter(basket=basket, item=item).delete()
    
    user = request.user
    if user.is_authenticated:
        email_user = user.emailuser
        try:
            registered_user = email_user.registereduser
        except RegisteredUser.DoesNotExist:
            is_registered = False
        else:
            order = Order.objects.get(user=email_user, is_payed=False,
                                      basket=basket)
            oi = OrderItem.objects.filter(order=order, item=item).delete()

    return redirect('basket')


@login_required
def order_add_comment(request, order_id):
    order = get_object_or_404(Order, pk=order_id)
    if order.user.user != request.user:
        raise Htt404
    if request.method == "POST":
        order_comment_form = OrderCommentForm(request.POST, instance=order)
        if order_comment_form.is_valid():
            order_comment_form.save()
            return HttpResponseRedirect(reverse('basket'))


def basket(request):
    coupon = False
    money_coupon = False
    coupon_form = CouponForm()
    if request.method == "GET":
        if request.GET.get('coupon_name', False):
            coupon_form = CouponForm(request.GET)
            if coupon_form.is_valid():
                coup = coupon_form.cleaned_data['coupon_name']
                try:
                    coupon = Coupon.objects.get(name=coup)
                except Coupon.DoesNotExist:
                    try:
                        money_coupon = MoneyCoupon.objects.get(title=coup)
                    except MoneyCoupon.DoesNotExist:
                        money_coupon = False
                        
    s_user = SUser(request)
    basket, c = Basket.objects.get_or_create(user=s_user.get_user)
    strong_package_price = 0

    basket_items = BasketItem.objects.filter(basket=basket)
    total = 0
    amount_items = 0
    for i in basket_items:
        amount_items += 1
        total += (i.item.get_price() * i.amount)
    if total < 1000: # cheap orders should pay for delivery
        delivery = 1000 - total
        total += 350 # FIXME
    else:
        delivery = False
    if not request.user.is_authenticated:
        is_authenticated = False
    else:
        is_authenticated = True
        user = request.user
        email_user = user.emailuser
        try:
            registered_user = email_user.registereduser
        except RegisteredUser.DoesNotExist:
            is_registered = False
            account_form = AccountForm()
        else:
            ru = email_user.registereduser 
            if ru.first_name and ru.last_name and ru.address:
                is_registered = True
            else:
                is_registered = False

            try:
                order, c = Order.objects.get_or_create(user=email_user, 
                                                       is_payed=False,
                                                           basket=basket)
            except Order.MultipleObjectsReturned:
                Order.objects.filter(user=email_user, is_payed=False,
                                     basket=basket).delete()
                order, c = Order.objects.get_or_create(user=email_user, 
                                                       is_payed=False,
                                                           basket=basket)
                
            for oi in OrderItem.objects.filter(order=order):
                good = False
                for i in basket_items:
                    if i.item == oi.item:
                        good = True
                if not good:
                    oi.delete()

            for i in basket_items:
                try:
                    oi = OrderItem.objects.get(item=i.item, order=order)
                except OrderItem.DoesNotExist:
                    oi, c = OrderItem.objects.get_or_create(item=i.item,
                                                            amount=i.amount,
                                                            order=order)
                except OrderItem.MultipleObjectsReturned:
                    OrderItem.objects.filter(item=i.item, order=order).delete()
                    oi, c = OrderItem.objects.get_or_create(item=i.item,
                                                            amount=i.amount,
                                                            order=order)

                    
            if order.strong_package:
                for i in basket_items:
                    strong_package_price += i.amount * 80 # 80 rub per package
            total += strong_package_price
            order.price = total
            order.save(update_fields=['price'])
            delta_total = 0
            # Now populate log
            ol_created = False
            while not ol_created:
                session_key = gen_url(240)
                try:
                    ol = OrderLog.objects.get(session_key=session_key)
                except OrderLog.DoesNotExist:
                    ol_created = True
                    ol = OrderLog(order=order, session_key=session_key)
                    ol.save()
                
            for bi in basket_items:
                oli = OrderLogItem(orderlog=ol, item=bi.item, amount=bi.amount)
                oli.save()
                
            if coupon:
                if coupon.min_sum < total:
                    old_total = total
                    order.coupon = coupon
                    order.save(update_fields=['coupon',])
                    total = int(total - total * (float(coupon.discount) / 100))
                    delta_total = old_total - total # Размер скидки в рублях
            if money_coupon:
                old_total = total
                order.money_coupon = money_coupon
                order.save(update_fields=['money_coupon',])
                total = total - money_coupon.amount
                if total <= 0:
                    money_coupon.spended = money_coupon.spended + old_total
                    money_coupon.amount = money_coupon.amount - old_total
                    money_coupon.save()
                    order.is_payed = True
                    order.save()
                    return HttpResponseRedirect(reverse("payment_success"))
                
                delta_total = old_total - total # Размер скидки в рублях

            order.coupon_delta = delta_total
            order.save(update_fields=["coupon_delta"])
                #oi.save()
            order_id = "%s_%s" % (order.pk, ol.pk)
            sign_str =  "%s:%s:%s:%s" % (settings.MERCHANT_ID, total, 
                                         settings.SECRET_KEY, order_id)
            m = hashlib.md5()
            m.update(sign_str.encode('utf8'))
            #print sign_str, m.hexdigest()
            if is_registered:
                phone = ru.phone
            else:
                phone = ""
            form = PaymentForm(initial={"m":settings.MERCHANT_ID, "oa":total, 
                                        "o":order_id, "s":m.hexdigest(),
                                        "phone":phone})
            order_comment_form = OrderCommentForm(instance=order)

    return render(request, 'user/basket.html', locals())


def basket_amount(request):
    s_user = SUser(request)
    try:
        basket = Basket.objects.get(user=s_user.get_user)
    except Basket.DoesNotExist:
        count = 0
    else:
        count = BasketItem.objects.filter(basket=basket).count()
    js = json.dumps({"amount":count})
    return HttpResponse(js, content_type='application/json')


def basket_amount_change(request):
    try:
        item_pk = int(request.GET.get('item', False))
        amount = int(request.GET.get('val', False))
    except ValueError:
        raise Http404
    s_user = SUser(request)
    basket = get_object_or_404(Basket, user=s_user.get_user)
    item = get_object_or_404(Item, pk=item_pk)
    bi = BasketItem.objects.get(basket=basket, item=item)
    bi.amount = amount
    bi.save(update_fields=['amount',])

    user = request.user
    if user.is_authenticated:
        email_user = user.emailuser
        try:
            registered_user = email_user.registereduser
        except RegisteredUser.DoesNotExist:
            is_registered = False
        else:
            order = Order.objects.get(user=email_user, is_payed=False,
                                      basket=basket)
            oi = OrderItem.objects.get(order=order, item=item)
            oi.amount = amount
            oi.save(update_fields=["amount",])
    
    js = json.dumps({"amount":amount})
    return HttpResponse(js, content_type='application/json')


@login_required
def orders_history(request):
    user = request.user
    email_user = user.emailuser
    orders = Order.objects.filter(user=email_user,
                                  is_deleted=False).order_by("-id")
    exclude_orders = []
    for order in orders:
        c = OrderItem.objects.filter(order=order).count()
        if c == 0 and order.is_payed == False:
            exclude_orders.append(order.pk)
    if len(exclude_orders) > 0:
        orders = Order.objects.filter(user=email_user,
                                      is_deleted=False)\
                              .exclude(pk__in=exclude_orders).order_by("-id")
        
    return render(request, "user/orders-history.html", {"orders": orders})


@login_required
def order_info(request, order_pk):
    user = request.user
    email_user = user.emailuser
    order = get_object_or_404(Order, user=email_user, pk=order_pk)
    shipments = order.shipments()
    shipments_count = len(shipments)
    order_items = OrderItem.objects.filter(order=order)
    total = order.price
    return render(request, "user/order-info.html",
                  {"order": order, "shipments": shipments,
                   "shipments_count": shipments_count,
                   "order_items": order_items})


def add_review(request, item_pk):
    item = get_object_or_404(Item, pk=item_pk)
    form = ReviewForm(request.POST)
    is_owner = False
    if request.POST.get('email', '') != '':
        # spam...
        return HttpResponseRedirect('/')
    
    if form.is_valid():
        instance = form.save(commit=False)
        instance.item = item
        user = request.user
        if user.is_authenticated:
            email_user = user.emailuser
            orders = Order.objects.filter(user=email_user, is_payed=True,
                                          is_deleted=False).order_by("-id")
            for order in orders:
                for oi in OrderItem.objects.filter(order=order):
                    if oi.item == item:
                        is_owner = True
            
        instance.is_owner = is_owner
        instance.save()
        try:
            instance.user = user.emailuser
            instance.save()
        except ValueError:
            print (traceback.format_exc())
            pass
        
    return_url = reverse('item', kwargs={"item_name":item.title,
                                         "item_pk":item.pk})
    return HttpResponseRedirect("%s?added=True" % return_url)


@login_required
def shipment_info(request, shipment_pk):
    shipment = get_object_or_404(Shipment, pk=shipment_pk)
    user = request.user
    if shipment.order.user.user != user:
        #print (shipment.order.user.user, user)
        raise Http404

    try:
        post_tr = PostTracker.objects.get(shipment=shipment)
    except PostTracker.DoesNotExist:
        post_tr = False
        post_tr_items = []
    else:
        post_tr_items = PostTrackerItem.objects.filter(posttracker=post_tr).order_by("date")
    return render(request, "user/shipment-info.html",
                  {"shipment": shipment, "post_tr": post_tr,
                   "post_tr_items": post_tr_items})
    

@login_required
def set_password(request):
    user = request.user
    saved = False
    if request.method == 'POST':
        form = PasswordForm(request.POST)
        if form.is_valid():
            password = form.cleaned_data['password1']
            user.set_password(password)
            user.save()
            saved = True
    else:
        form = PasswordForm()
    return render(request, "user/set-password.html", {"form": form, "saved": saved})


def basket_strong_package(request):
    try:
        val = str(request.GET.get('val', 'false'))
    except ValueError:
        raise Http404
    #print (val, type(val))
    if val == "false":
        val = False
    else:
        val = True
        
    s_user = SUser(request)
    basket = get_object_or_404(Basket, user=s_user.get_user)

    user = request.user
    if user.is_authenticated:
        email_user = user.emailuser
        order = Order.objects.get(user=email_user, is_payed=False, basket=basket)
        order.strong_package = val
        order.save(update_fields=["strong_package",])
    else:
        raise Http404

    js = json.dumps({"result": 1})
    return HttpResponse(js, content_type='application/json')
