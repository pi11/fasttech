#-*- coding: utf-8 -*-

from django.contrib import admin

from .models import *

class OrderAdmin(admin.ModelAdmin):
    pass

admin.site.register(Order, OrderAdmin)

class CountryAdmin(admin.ModelAdmin):
    pass

admin.site.register(Country, CountryAdmin)


class CouponAdmin(admin.ModelAdmin):
    pass

admin.site.register(Coupon, CouponAdmin)

class MoneyCouponAdmin(admin.ModelAdmin):
    pass

admin.site.register(MoneyCoupon, MoneyCouponAdmin)


class PostTrackerAdmin(admin.ModelAdmin):
    pass

admin.site.register(PostTracker, PostTrackerAdmin)

class RegisteredUserAdmin(admin.ModelAdmin):
    list_display  = ("last_name", "first_name", "get_phone")
    search_fields = ("user__user__email", "first_name", "last_name")

admin.site.register(RegisteredUser, RegisteredUserAdmin)


class ShipmentAdmin(admin.ModelAdmin):
    pass

admin.site.register(Shipment, ShipmentAdmin)
