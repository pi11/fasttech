# -*- coding: utf-8 -*-

from datetime import datetime
import re
import pytz

from django.db import models
from djangohelpers.utils import gen_url
from django.db.models.signals import pre_save
from django.urls import reverse
from django.conf import settings

from emailuser.models import EmailUser
from shop.models import Item
from emailservice.models import EmailObject, SmsObject


class MoneyCoupon(models.Model):
    title = models.CharField(max_length=100, null=True, blank=True, 
                             verbose_name="Купон")
    added = models.DateTimeField(auto_now_add=True)
    amount = models.IntegerField(default=0, verbose_name="Сумма купона") # сумма купона в рублях
    spended = models.IntegerField(default=0, verbose_name="Потрачено") # потрачено в копейках
    
    def __str__(self):
        return self.title


class Coupon(models.Model):
    title = models.CharField(max_length=100, null=True, blank=True, 
                             verbose_name="Название купона")
    name = models.CharField(max_length=100, unique=True, verbose_name="Купон")
    added = models.DateTimeField(auto_now_add=True)
    start_date = models.DateField(verbose_name="Дата начала действия")
    end_date = models.DateField(verbose_name="Дата окончения действия")
    used_count = models.IntegerField(default=0)
    discount = models.IntegerField(default=0, 
                                   verbose_name="Скидка в процентах")
    min_sum = models.IntegerField(default=0,
                                  verbose_name="Минимальная сумма заказа")
    max_used = models.IntegerField(default=1000000,
                                   verbose_name="Сколько раз можно использовать")
    
    def __str__(self):
        if self.title:
            return self.title
        return self.name


class SessionUser(models.Model):
    """User UID stored in Cookies"""
    uid = models.CharField(max_length=150, unique=True)
    publication_date = models.DateTimeField(auto_now_add=True)
    last_used = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.uid


class Country(models.Model):
    name = models.CharField(max_length=100, unique=True)
    is_default = models.BooleanField(default=False)
    is_empty = models.BooleanField(default=False)

    class Meta:
        ordering = ("-is_default", "name")

    def __str__(self):
        return self.name


class RegisteredUser(models.Model):
    user = models.OneToOneField(EmailUser, on_delete=models.PROTECT,
                                editable=False)
    s_user = models.OneToOneField(SessionUser, null=True,
                                  blank=True, on_delete=models.PROTECT,
                                      editable=False)

    first_name = models.CharField(max_length=100, verbose_name="Имя")
    last_name = models.CharField(max_length=100, verbose_name="Фамилия")
    patronymic = models.CharField(max_length=150, verbose_name="Отчество", 
                                  null=True, blank=True)
    phone = models.CharField(max_length=100, verbose_name="Телефон")
    zip = models.CharField(max_length=20, null=True, blank=True, 
                           verbose_name="Индекс")
    country = models.ForeignKey(Country, null=True, blank=True,
                                verbose_name="Выберету страну доставки",
                                on_delete=models.PROTECT)
    country_added = models.CharField(max_length=100, null=True, blank=True,
                                     verbose_name="Если страны нет в списке, введите название тут")
    
    city = models.CharField(max_length=100, null=True, blank=True, 
                            verbose_name="Город/Поселок")
    address = models.CharField(max_length=350, verbose_name="Адрес")
    score = models.IntegerField(default=0)
    bonus = models.IntegerField(default=0)

    is_baned = models.BooleanField(default=False)
    # Рейтинг юзверя
    
    def __str__(self):
        return str(self.user)

    def full_name(self):
        return "%s %s %s" % (self.last_name, self.first_name, self.patronymic)
                        
    def get_phone(self):
        phone = "".join(filter(lambda x: x.isdigit(), self.phone))
        if len(phone) > 1:
            if phone[0] == "7":
                phone = "+%s" % phone
            if phone[0] == "8":
                phone = "+7%s" % phone[1:]
        return phone


class Basket(models.Model):
    """Корзина пользователя"""
    user = models.OneToOneField(SessionUser, on_delete=models.CASCADE)
    updated = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.user)


class BasketItem(models.Model):
    basket = models.ForeignKey(Basket, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    amount = models.IntegerField(default=1)

    def get_total_price(self):
        return self.item.get_price() * self.amount

    def __str__(self):
        return str(self.item)


class PostTracker(models.Model):
    shipment = models.ForeignKey('Shipment', null=True, blank=True,
                                 on_delete=models.PROTECT)
    last_status = models.CharField(max_length=250, null=True, blank=True)
    is_delivered = models.BooleanField(default=False)
    is_recieved = models.BooleanField(default=False)
    last_check = models.DateTimeField(auto_now_add=True)
    delivery_date = models.DateTimeField(null=True, blank=True)
    
    def __str__(self):
        return str(self.pk) #self.last_status


class Shipment(models.Model):
    order = models.ForeignKey('Order', on_delete=models.PROTECT)
    tracking = models.CharField(max_length=40, null=True, blank=True,
                                verbose_name="Трэк-номер")
    orderitem = models.ManyToManyField('OrderItem',
                                       verbose_name="Товары в отправлении")
    photo = models.ImageField(upload_to="shipments/", null=True, blank=True,
                              verbose_name="Фото посылки")
    track_status = models.CharField(max_length=150, null=True, blank=True)
    added = models.DateTimeField(auto_now_add=True)
    last_check = models.DateTimeField(auto_now=True)
    is_recieved = models.BooleanField(default=False)
    is_delivered = models.BooleanField(default=False)

    def __str__(self):
        if self.tracking:
            return self.tracking
        else:
            return str(self.pk)
     
    def get_track_status(self):
        if self.track_status:
            return self.track_status
        else:
            return self.tracking

    def get_total_time(self):
        """Return time in days from send to delivery"""
        if self.is_delivered:
            dd = PostTracker.objects.get(shipment=self).delivery_date
            if not dd:
                return ""
            return (dd - self.added).days
        else:
            return (datetime.now(pytz.utc) - self.added).days


class OrderItem(models.Model):
    order = models.ForeignKey('Order', on_delete=models.PROTECT)
    item = models.ForeignKey(Item, on_delete=models.PROTECT)
    amount = models.IntegerField(default=1)

    def get_total_price(self):
        return self.item.get_price() * self.amount
    
    def __str__(self):
        return str(self.item)


class Order(models.Model):
    basket = models.ForeignKey('Basket', on_delete=models.SET_NULL,
                               null=True, blank=True)
    user = models.ForeignKey(EmailUser, on_delete=models.PROTECT)
    updated = models.DateTimeField(auto_now_add=True)
    pay_date = models.DateTimeField(null=True, blank=True, default=None)
    is_payed = models.BooleanField(default=False, verbose_name="Заказ оплачен")
    is_done = models.BooleanField(default=False, verbose_name="Заказ выполнен")
    is_sended = models.BooleanField(default=False, verbose_name="Заказ выслан")
    is_processed = models.BooleanField(default=False, 
                                       verbose_name="Заказ обработан")

    price = models.CharField(max_length=100, null=True, blank=True)

    manager_remark = models.TextField(null=True, blank=True,
                                      verbose_name="Заметки о заказе")
    
    coupon = models.ForeignKey(Coupon, null=True, blank=True,
                               on_delete=models.PROTECT)
    money_coupon = models.ForeignKey(MoneyCoupon, null=True, blank=True,
                               on_delete=models.PROTECT)

    is_recieved = models.BooleanField(default=False)

    estimated_send_date = models.DateField(null=True, blank=True,
                                           verbose_name="Расчетная дата отправки")
    # устанавливается менеджером
    is_deleted = models.BooleanField(default=False, verbose_name="Удалить")

    bonus_recieved = models.BooleanField(default=False)

    user_comment = models.TextField(null=True, blank=True,
                                    verbose_name="Комментарий к заказу")
    strong_package = models.BooleanField(default=False)
    
    total_payed = models.IntegerField(default=0)
    coupon_delta = models.IntegerField(default=0)

    fasttech_order_url = models.CharField(max_length=150, null=True, blank=True,
                                          verbose_name="Ссылка на заказ")

    def get_estimate_send_date(self):
        # расчет примерной даты доставки
        v = 0
        for i in OrderItem.objects.filter(order=self):
            iv = i.item.estimate_send_date()
            if iv > v:
                v = iv
        return v + 1  # return max estimated time (in days) for order

    def get_estimate_send_date_str(self):
        esd = self.get_estimate_send_date() + 1
        if esd < 2:
            res = "1 день"
        elif esd in [2, 3, 4,]:
            res = "%s дня" % esd
        else:
            res = "%s дней" % esd
        return res
    
    def total_orders(self):
        return Order.objects.filter(is_payed=True, user=self.user).count()
    
    def get_status(self):
        if self.is_deleted:
            return "Заказ удален"
        
        if self.is_done:
            return "Выполнен"
        if self.is_sended:
            return "Выслан"
        if self.is_processed:
            return "Обработан, заказ собирается"
        if self.is_payed:
            return "Оплачен, готовится"
        if self.is_recieved:
            return "Получен клиентом"
        return "Ожидается оплата"

    def get_status_css(self):
        if self.is_done:
            return "order_done"
        if self.is_sended:
            return "order_sended"
        if self.is_processed:
            return "order_processed"
        if self.is_payed:
            return "order_payed"
        return "Ожидается оплата"

    def shipments(self):
        return Shipment.objects.filter(order=self)
    
    def __str__(self):
        return str(self.pk)


class SUser:
    """Session User (tmp user)"""
    def __init__(self, request):
        uid = request.session.get('tmpuser', False)
        if uid == False:
            new_uid = gen_url(150)
            while SessionUser.objects.filter(uid=new_uid).count() > 0:
                new_uid = gen_url(100)
            su = SessionUser(uid=new_uid)
            su.save()
            request.session['tmpuser'] = new_uid
            self.uid = new_uid
        else:
            self.uid = uid
        #d = timedelta(days = 3650)
        #request.session.set_expiry(d)
        try:
            su = SessionUser.objects.get(uid=uid)
        except SessionUser.DoesNotExist:
            su = SessionUser(uid=uid)
            su.save()
        self.suser = su
        return

    @property
    def get_user(self):
        return self.suser


class PostTrackerItem(models.Model):
    date = models.CharField(null=True, blank=True, max_length=100)
    posttracker = models.ForeignKey(PostTracker, on_delete=models.PROTECT)
    status = models.CharField(max_length=250)
    info = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.status

    def get_date(self):
        # FIXME: convert string to date
        return self.date


class Review(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    nickname = models.CharField(max_length=80, null=True, blank=True,
                                verbose_name="Ваше имя")
    user = models.ForeignKey(EmailUser, null=True, blank=True,
                             on_delete=models.CASCADE)
    text = models.TextField(verbose_name="Отзыв")
    added = models.DateTimeField(auto_now_add=True)
    is_moderated = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_owner = models.BooleanField(default=True)
    
    def __str__(self):
        if self.nickname:
            return self.nickname
        else:
            return str(self.user)

    def get_nickname(self):
        if self.user:
            return str(self.user)
        else:
            return self.nickname


class VideoReview(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    youtube_video = models.CharField(max_length=100)
    added = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)
    is_moderated = models.BooleanField(default=False)

    def __str__(self):
        return self.youtube_video

    def get_embed_code(self):
        youtube_id_re = re.compile('watch\?v=([a-zA-Z0-9_\-]+)')
        try:
            youtube_id = youtube_id_re.findall(self.youtube_video)[0]
        except IndexError:
            return ""
        return "https://www.youtube.com/embed/%s" % youtube_id
    

class OrderLog(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    session_key = models.CharField(max_length=250, unique=True)
    date = models.DateTimeField(auto_now_add=True)
    is_payed = models.BooleanField(default=False)


class OrderLogItem(models.Model):
    orderlog = models.ForeignKey(OrderLog, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.PROTECT)
    amount = models.IntegerField(default=1)


def order_changed_email(sender, instance, **kwargs):
    """ Send email and sms to user about order status changed"""
    if settings.DEBUG:
        return # Ignore sending message for debug version
    try:
        obj = sender.objects.get(pk=instance.pk)
    except sender.DoesNotExist:
        pass # Object is new, pass
    else:
        text = False
        if (not obj.is_payed == instance.is_payed):
            text = ("Ваш заказ #%s оплачен.\n" 
                    "В ближайшее время он будет обработан и выслан"
                    " на Ваш адрес. \nhttp://fastek.link/") % obj.pk

        if (not obj.is_done == instance.is_done):
            text = "Ваш заказ #%s выполнен.\n"  % obj.pk
            text = False # issue #40

        #if (not obj.is_sended == instance.is_sended):
        #    text = "Ваш заказ #%s выслан на Ваш адрес!\n"  % obj.pk

        if (not obj.is_processed == instance.is_processed):
            if obj.estimated_send_date:
                est_send_date = obj.estimated_send_date
            else:
                est_send_date = obj.get_estimate_send_date_str()
                
            text = ("Ваш заказ #%s обработан и в данный момент "
                    "собирается. \n В ближайшее время он будет выслан"
                    "\nОриентировочное время отправки заказа: %s\n\n"
                    "(Но не менее, 2 дней). Обратите внимание, что "
                    "расчётное время указано при условии, что весь "
                    "товар на нашем складе. Ожидайте уведомление с трекномером.\n"
                    "\nhttp://fastek.link/")  % (obj.pk, est_send_date)

        #if (not obj.tracking == instance.tracking and instance.tracking != '' \
        #    and instance.tracking is not None):
        #    tracking = 
        #    text = ("Ваш заказ #%s выслан на Ваш адрес!\n"
        #            "Трэк номер для отслеживания доставки: %s ") % (obj.pk, instance.tracking)
        #    so, c = SmsObject.objects.get_or_create(
        #        to=obj.user.registereduser.get_phone(),
        #        text=("Ваш заказ #%s на сайте fastek.link выслан!\n"
        #              "Трэк номер для отслеживания доставки: %s " % (obj.pk, instance.tracking)))

        if text:
            eo, c = EmailObject.objects.get_or_create(
                subject="Статус заказа #%s на fastek.link" % obj.pk,
                text=text, to=obj.user.user.email)

            try:
                so, c = SmsObject.objects.get_or_create(
                    to=obj.user.registereduser.get_phone(),
                    text=text
                )
            except:
                print (traceback.format_exc())

pre_save.connect(order_changed_email, sender=Order)


def shipment_changed_email(sender, instance, **kwargs):
    """ Send email to user about shipment added"""
    if settings.DEBUG:
        return # Ignore sending message for debug version
    try:
        obj = sender.objects.get(pk=instance.pk)
    except sender.DoesNotExist:
        text = ("Ваш заказ #%s выслан на Ваш адрес!\n"
                "Трэк номер для отслеживания доставки: %s ") % (instance.order.pk, instance.tracking)
        so, c = SmsObject.objects.get_or_create(
            to=instance.order.user.registereduser.get_phone(), text=text)

        eo, c = EmailObject.objects.get_or_create(
            subject="Статус заказа #%s на fastek.link" % instance.order.pk,
            text=text, to=instance.order.user.user.email)


pre_save.connect(shipment_changed_email, sender=Shipment)
