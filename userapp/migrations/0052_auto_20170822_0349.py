# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-08-22 03:49
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userapp', '0051_auto_20170819_2225'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='support',
            name='order',
        ),
        migrations.RemoveField(
            model_name='support',
            name='user',
        ),
        migrations.RemoveField(
            model_name='supportitem',
            name='support',
        ),
        migrations.DeleteModel(
            name='Support',
        ),
        migrations.DeleteModel(
            name='SupportItem',
        ),
    ]
