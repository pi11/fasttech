# Generated by Django 2.0.1 on 2018-01-22 01:50

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userapp', '0061_order_to_pay'),
    ]

    operations = [
        migrations.RenameField(
            model_name='order',
            old_name='to_pay',
            new_name='coupon_delta',
        ),
    ]
