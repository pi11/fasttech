# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-08-09 22:34
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('userapp', '0045_auto_20170803_2308'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shipment',
            name='photo',
            field=models.ImageField(blank=True, null=True, upload_to='shipments/', verbose_name='Фото посылки'),
        ),
    ]
