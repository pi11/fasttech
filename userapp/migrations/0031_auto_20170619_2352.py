# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-19 23:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('userapp', '0030_auto_20170619_2344'),
    ]

    operations = [
        migrations.AlterField(
            model_name='review',
            name='text',
            field=models.CharField(max_length=2512),
        ),
    ]
