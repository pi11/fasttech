# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-08-19 01:55
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0051_auto_20170819_0155'),
        ('userapp', '0048_order_bonus_recieved'),
    ]

    operations = [
        migrations.CreateModel(
            name='VideoReview',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('youtube_video', models.CharField(max_length=100)),
                ('added', models.DateTimeField(auto_now_add=True)),
                ('is_active', models.BooleanField(default=False)),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='shop.Item')),
            ],
        ),
    ]
