import re, os
import sys

from django.core.management.base import BaseCommand, CommandError
from django.urls import reverse

from userapp.models import Coupon, Order

class Command(BaseCommand):
    help = 'Update Coupon stats (used times)'

    def handle(self, *args, **options):
        for c in Coupon.objects.filter():
            c.used_count = Order.objects.filter(coupon=c, is_payed=True).count()
            c.save()
