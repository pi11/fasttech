import re, os
import sys

from django.core.management.base import BaseCommand, CommandError
from django.urls import reverse
from django.conf import settings

from userapp.models import VideoReview
from shop.models import Item

class Command(BaseCommand):
    help = 'Process Orders and add bonuses'

    def handle(self, *args, **options):

        from apiclient.discovery import build
        from apiclient.errors import HttpError
        from oauth2client.tools import argparser


        # Set DEVELOPER_KEY to the API key value from the APIs & auth > Registered apps
        # tab of
        #   https://cloud.google.com/console
        # Please ensure that you have enabled the YouTube Data API for your project.
        DEVELOPER_KEY = settings.YOUTUBE_API_KEY
        YOUTUBE_API_SERVICE_NAME = "youtube"
        YOUTUBE_API_VERSION = "v3"

        def youtube_search(q):
            youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
                            developerKey=DEVELOPER_KEY)

            # Call the search.list method to retrieve results matching the specified
            # query term.
            search_response = youtube.search().list(
                q=q,
                part="id",
                maxResults=1
            ).execute()

            videos = []
            for search_result in search_response.get("items", []):
                if search_result["id"]["kind"] == "youtube#video":
                    videos.append(search_result["id"]["videoId"])
            if len(videos) == 0:
                if "обзор" in q:
                    print ("Trying eng version")
                    videos = youtube_search(q.replace('обзор', ''))
                    
            print (videos)
            return videos
        for i in Item.objects.filter(is_active=True, is_video_review_parsed=False).order_by("-price")[:100]:
            try:
                youtube_id = youtube_search("%s обзор" % i.title.replace("Authentic", ""))[0]
            except IndexError:
                print ("No video found: %s" % i.title)
                continue
            i.is_video_review_parsed = True
            i.save(update_fields=['is_video_review_parsed',])
            url = "https://www.youtube.com/watch?v=%s" % youtube_id
            vr, c = VideoReview.objects.get_or_create(youtube_video=url, item=i)
            if c:
                print ("Added new video: %s" % url)
