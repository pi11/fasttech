import re, os
import sys

from django.core.management.base import BaseCommand, CommandError
from django.urls import reverse

from userapp.models import RegisteredUser, Order

class Command(BaseCommand):
    help = 'Process Orders and add bonuses'

    def handle(self, *args, **options):
        for o in Order.objects.filter(is_recieved=True,
                                      bonus_recieved=False):
            o.user.registereduser.bonus += int(o.price) * 10
            o.user.registereduser.save(update_fields=['bonus',])
            o.bonus_recieved = True
            o.save(update_fields=['bonus_recieved',])
            print ("User:%s, bonuses: %s" % (o.user.registereduser.first_name,
                   o.user.registereduser.bonus))
            
