# -*- coding: utf-8 -*-
from django import forms
from django.forms import ModelForm, Form


from .models import Question, Response



class QuestionForm(ModelForm):
    class Meta:
        model = Question
        fields = ("title", "description", )

class ResponseForm(ModelForm):
    class Meta:
        model = Response
        fields = ("description", )

class QuestionItemForm(ModelForm):
    class Meta:
        model = Question
        fields = ("description", "item")
        widgets = {
            'item': forms.HiddenInput(),
            'description': forms.Textarea(attrs={'class':
                                                 'question_form_field',
                                                  })
        }

