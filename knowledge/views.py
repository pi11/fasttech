# -*- coding: utf-8 -*-

from django.db.models import Q
from django.shortcuts import render, get_object_or_404
from django.http import Http404, HttpResponseRedirect
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from django.views import View

from shop.functions import prepare_page
from .models import *
from .forms import QuestionForm, ResponseForm, QuestionItemForm


def index(request, action='all'):
    items = Question.objects.filter().order_by("-pk")
    if request.user.is_authenticated:
        if action == "my":
            items = items.filter(user=request.user)
        elif not request.user.is_staff:
            items = items.filter(Q(is_public=True)|Q(user=request.user))
    else:
        items = items.filter(is_public=True)
        
    page, page_range = prepare_page(request, items)
    return render(request, "knowledge/index.html",
                  {"page":page, "page_range":page_range,
                   "action":action})
    

def category(request, category_id):
    category = get_object_or_404(Category, pk=category_id)
    items = Question.objects.filter(is_public=True, categories=category)
    page, page_range = prepare_page(request, items)
    return render(request, "knowledge/category.html",
                  {"page":page, "page_range":page_range,})


def question(request, question_id):
    q = get_object_or_404(Question, pk=question_id)
    print (q.item)
    if not q.is_public:
        if request.user.is_authenticated and not request.user.is_staff:
            if q.user != request.user:
                raise Http404
            
    responses = Response.objects.filter(question=q,
                                        accepted=True).order_by('added')
    if request.user.is_staff:
        responses = Response.objects.filter(question=q).order_by('added')
        
    return render(request, "knowledge/question.html",
                  {"q":q, "responses":responses, "form":ResponseForm()})


def response(request, question_id):
    q = get_object_or_404(Question, pk=question_id)
    form = ResponseForm(request.POST)
    if form.is_valid():
        resp = form.save(commit=False)
        resp.question = q
        resp.user = request.user
        if request.user.is_staff:
            resp.accepted = True
        resp.save()
        return HttpResponseRedirect(
            reverse('knowledge:question', kwargs={"question_id":q.pk}))
    raise Http404

    
class AskView(View):
    form_class = QuestionForm
    template_name = 'knowledge/ask.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name,
                      {'form': QuestionForm()})

    def post(self, request, *args, **kwargs):
        if request.POST.get('item', False):
            form = QuestionItemForm(request.POST)
        else:
            form = QuestionForm(request.POST)
        if form.is_valid():
            q = form.save(commit=False)
            q.user = request.user
            q.save()
            return HttpResponseRedirect(
                reverse('knowledge:question', kwargs={"question_id":q.pk}))

        return render(request, self.template_name, {'form': form})


@staff_member_required
def response_moderate(request, response_id, action):
    resp = get_object_or_404(Response, pk=response_id)
    if action == "hide":
        resp.accepted = False
    if action == "accept":
        resp.accepted = True
    resp.save()
    return HttpResponseRedirect(
        reverse('knowledge:question', kwargs={"question_id":resp.question.pk}))


@staff_member_required
def question_moderate(request, question_id, action):
    q = get_object_or_404(Question, pk=question_id)
    if action == "hide":
        q.is_public = False
    if action == "accept":
        q.is_public = True
        
    if action == "open":
        q.is_closed = False
    if action == "close":
        q.is_closed = True
    q.save()
    return HttpResponseRedirect(
        reverse('knowledge:question', kwargs={"question_id":q.pk}))
    

@login_required
def question_edit(request, question_id):
    q = get_object_or_404(Question, pk=question_id)
    if request.user != q.user and not request.user.is_staff:
        raise Http404
    if request.method == "POST":
        form = QuestionForm(request.POST, instance=q)
        if form.is_valid():
            q = form.save()
            return HttpResponseRedirect(
                reverse('knowledge:question', kwargs={"question_id":q.pk}))
    else:
        form = QuestionForm(instance=q)
    

    return render(request, "knowledge/question-edit.html",
                  {'form': form, "question": q})


@login_required
def response_edit(request, response_id):
    resp = get_object_or_404(Response, pk=response_id)
    if request.user != resp.question.user and not request.user.is_staff:
        raise Http404
    if request.method == "POST":
        form = ResponseForm(request.POST, instance=resp)
        if form.is_valid():
            resp = form.save()
            return HttpResponseRedirect(
                reverse('knowledge:question',
                        kwargs={"question_id": resp.question.pk}))
    else:
        form = ResponseForm(instance=resp)
    

    return render(request, "knowledge/response-edit.html",
                  {'form': form, "response": resp})
    

@staff_member_required
def question_delete(request, question_id):
    q = get_object_or_404(Question, id=question_id)
    resp = Response.objects.filter(question=q).delete()
    q.delete()
    return HttpResponseRedirect(reverse('knowledge:index'))
