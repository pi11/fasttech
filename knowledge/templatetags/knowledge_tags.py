import markdown
from hashlib import md5
from urllib.parse import urlencode
from django.template import Library, Node
from django.conf import settings

from knowledge.models import Category


register = Library()

@register.simple_tag
def get_gravatar(email, size=60, rating='g', default=None):
    """ Return url for a Gravatar. From Zinnia blog. """
    url = 'https://secure.gravatar.com/avatar/{0}.jpg'.format(
        md5(email.strip().lower().encode('utf8')).hexdigest()
    )
    options = {'s': size, 'r': rating}
    if default:
        options['d'] = default

    url = '%s?%s' % (url, urlencode(options))
    return url.replace('&', '&amp;')

@register.filter
def markdown_filter(text):
    return markdown.markdown(text)


def build_categories_list(parser, token):
    """
    {% get_knowledge_category_list %}
    """
    return CategoryMenuObject()

class CategoryMenuObject(Node):
    def render(self, context):
        categories_list = Category.objects.filter(is_active=False).order_by("title")
        context['knowledge_categories_list'] = categories_list
        return ''

register.tag('get_knowledge_categories_list', build_categories_list)


