from django.conf.urls import url
from .views import *

app_name = "knowledge"
urlpatterns = [
    url(r'^$', index, name="index"),
    url(r'^(?P<action>my)/$', index, name="index"),
    url(r'^ask/$', AskView.as_view(), name="ask"),
    url(r'^category/(?P<category_id>\d+)/$', category, name="category"),
    url(r'^question/(?P<question_id>\d+)/$', question, name="question"),
    url(r'^question/(?P<question_id>\d+)/delete/$', question_delete, name="question_delete"),

    url(r'^question/(?P<question_id>\d+)/edit/$', question_edit, name="question_edit"),

    url(r'^response/(?P<response_id>\d+)/edit/$', response_edit, name="response_edit"),
    url(r'^response/(?P<question_id>\d+)/$', response, name="response"),
    url(r'^response/moderate/(?P<response_id>\d+)/(?P<action>hide|accept)/$',
        response_moderate, name="response_moderate"),
    url(r'^question/moderate/(?P<question_id>\d+)/(?P<action>hide|accept|open|close)/$',
        question_moderate, name="question_moderate"),
]
