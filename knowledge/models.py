# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save, pre_save
from django.conf import settings
from django.urls import reverse

from djangohelpers.utils import wilson_score

from emailservice.models import EmailObject
from shop.models import Item


class Category(models.Model):
    """Разделы"""
    title = models.CharField(max_length=255)
    added = models.DateTimeField(auto_now_add=True)
    last_changed = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)
    
    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['title']
        verbose_name = 'Раздел'
        verbose_name_plural = "Разделы"

        
class BaseModel(models.Model):
    """
    The base class for our models.
    """

    added = models.DateTimeField(auto_now_add=True)
    last_changed = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    is_public = models.BooleanField(default=False)

    vote_plus = models.IntegerField(default=0)
    vote_minus = models.IntegerField(default=0)
    rate = models.FloatField(default=0)


    class Meta:
        abstract = True
        ordering = ['-added']
    
    def save(self, *args, **kwargs):
        if self.vote_plus + self.vote_minus > 0:
            total_votes = self.vote_plus + self.vote_minus
            sum_rate = self.vote_plus
            self.rate = wilson_score(sum_rate, total_votes, [0,1,])
        
        super(BaseModel, self).save(*args, **kwargs)
    
    def __str__(self):
        return self.title

        
class Response(BaseModel):
    description = models.TextField(verbose_name="Ответ",
                                   help_text="Можно использовать markdown")
    
    accepted = models.BooleanField(default=False)
    question = models.ForeignKey('Question', on_delete=models.CASCADE)
    
    class Meta:
        ordering = ['-added']
        verbose_name = "Ответ"
        verbose_name_plural = "Ответы"

        
class Question(BaseModel):
    title = models.CharField(max_length=250, verbose_name="Вопрос",
                             help_text="Введите кратко ваш вопрос",)
    description = models.TextField(verbose_name="Описание вопроса",
                                   help_text="Опишите подробнее вопрос, можно использовать markdown")
    
    is_closed = models.BooleanField(default=False) # ответы больше не принимаются
    item = models.ForeignKey(Item, null=True, blank=True,
                             on_delete=models.CASCADE)
    categories = models.ManyToManyField(Category)

    class Meta:
        verbose_name = "Вопрос"
        verbose_name_plural = "Вопросы"

    def response_count(self):
        return Response.objects.filter(accepted=True, question=self).count()

    def response_count_str(self):
        c = self.response_count()
        print (c)
        res = ""
        if c == 0:
            res = " нет ответов"
        elif c in range(5, 21):
            res = "%s ответов" % c
        elif str(c)[-1] == "1":
            res = "%s ответ" % c
        elif int(str(c)[-1]) in [2, 3, 4,]:
            res = "%s ответа" % c
        return res


def new_question(sender, instance, created, **kwargs):
    """ Send email to admins about new question added"""
    if settings.DEBUG:
        return # Ignore sending message for debug version

    if created:
        url = reverse('knowledge:question',
                      kwargs={"question_id":instance.id})
        text = "Новый вопрос в поддержку: %s%s" % (settings.SITE_URL, url)
        for m in settings.MANAGERS:
            eo, c = EmailObject.objects.get_or_create(
                subject="Новый вопрос в поддержку на fastek.link",
                text=text, to=m[1])
        
post_save.connect(new_question, sender=Question)

def new_response(sender, instance, created, **kwargs):
    """ Send email to user about new response added"""
    if settings.DEBUG:
        return # Ignore sending message for debug version

    if not created:
        return

    url = reverse('knowledge:question',
                  kwargs={"question_id":instance.question.id})
    text = "Ваш вопрос получил новый ответ: %s%s"\
           % (settings.SITE_URL, url)

    if instance.user == instance.question.user:
        for m in settings.MANAGERS:
            text = "Новый ответ на fastek.link %s%s"\
                   % (settings.SITE_URL, url)
            eo, c = EmailObject.objects.get_or_create(
                subject="Новый ответ на fastek.link",
                text=text, to=m[1])
        
    else:
        eo, c = EmailObject.objects.get_or_create(
            subject="Новый ответ на сайте %s" % settings.SITE_URL,
            text=text, to=instance.question.user.email)

post_save.connect(new_response, sender=Response)

