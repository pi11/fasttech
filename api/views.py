# -*- coding: utf-8 -*-

import re
import json
from datetime import timedelta, datetime
from pyquery import PyQuery as pq
import traceback
import requests
import time
import xml.etree.ElementTree as ET
import hashlib
# import cfscrape
import cloudscraper

from django.utils import timezone
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.core.exceptions import PermissionDenied
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.urls import reverse

from djangohelpers.utils import get_client_ip
from shop.models import Item, Stock
from .forms import PaymentSuccessForm, PaymentPushError
from userapp.models import Order, SUser, Basket, BasketItem, OrderLog
from emailservice.models import EmailObject # , SmsObject


def check_stock_f(item_id):
    item = get_object_or_404(Item, pk=item_id)
    result = {}
    result['stock'] = item.stock.name
    result['stock_i'] = item.stock.avail
    result['stock_q'] = False
    if item.stock.name == "В наличии":
        result['stock_q'] = True
        
    if item.stock.name == "in_stock":
        result['stock_q'] = True
        
    if item.stock_last_check + timedelta(hours=12) < timezone.now():
        result['stock_q'] = False
        
        # need check the stock!
        #scraper = cfscrape.create_scraper()  # returns a CloudflareScraper instance
        scraper = cloudscraper.create_scraper()
        r = scraper.get(item.url.url)
        if r.status_code != 200:
            print('Cant load data')
        else:
            data = r.content
            parsed = pq(data)
            parsed.make_links_absolute(base_url='http://www.fasttech.com')
            stock_name = parsed('.ProductOptions').find('meta[itemprop="availability"]').attr('content')

            stock_days = 0
            if not stock_name or stock_name == "":
                item.is_active = False
                result['stock_i'] = -1
                result['stock'] = u"Нет в наличии"
                item.save()
            else:
                prod_options = parsed('.ProductOptions').items('td')
                is_next = False
                stock_re0 = re.compile(r'ETA:(.*?) \((\d+) days\)')
                
                stock_re = re.compile(r'Ships in (\d+) business')
                stock_re2 = re.compile(r'Ships in (\d+) to (\d+) business days')

                stock_check = ""
                avail = 1
                for po in prod_options:
                    if is_next:
                        stock_eta = po('a').text().strip()
                        #print ("ETA:%s" % stock_eta)
                        try:
                            res = int(stock_re0.findall(stock_eta)[0][1])
                            
                        except (IndexError, ValueError):
                            found_eta = False
                        else:
                            found_eta = True
                            stock_days = res + 1
                            stock_check = "Со склада ~%s дней" % res
                        if not found_eta:
                            if "In stock" in stock_eta:
                                # print ("In Stock!!!!")
                                stock_check = "В наличии"
                                result['stock_q'] = True
                                
                            elif "Discontinued" in stock_eta:
                                stock_check = "Нет в наличии"
                                avail = -1
                            elif "out_of_stock" in stock_eta:
                                stock_check = "Нет в наличии"
                                avail = -1
                            elif "next day" in stock_eta or "today" in stock_eta:
                                stock_check = "В наличии"
                                result['stock_q'] = True

                            elif "Pre-order" in stock_eta:
                                stock_check = "Предзаказ"
                                stock_days = 10 #
                            elif "Back ordered" in stock_eta.lower():
                                stock_check = "Ожидается на склад"
                            elif "Temporarily sold out" in stock_eta:
                                stock_check = "Временно распродано"
                                avail = -1
                            else:
                                try:
                                    res = int(stock_re.findall(stock_eta)[0])
                                except IndexError:
                                    try:
                                        res, res2 = stock_re2.findall(stock_eta)[0]
                                    except IndexError:
                                        stock_check = "Со склада ~7 дней"
                                        stock_days = 7
                                    else:

                                        if int(res2) in [2, 3, 4]:
                                            stock_check = "Со склада %s-%s дня" % (res, res2)
                                        else:
                                            stock_check = "Со склада %s-%s дней" % (res, res2)
                                        stock_days = res2
                                else:
                                    if res in [2, 3, 4]:
                                        stock_check = "Со склада %s дня" % res
                                    else:
                                        stock_check = "Со склада %s дней" % res
                                    stock_days = res

                        break
                    if po.text().strip() == "stock":
                        is_next = True

                #print (stock_check, stock_days)
                stock, c = Stock.objects.get_or_create(name=stock_check)
                stock.avail = avail
                stock.save()
                result['stock'] = stock_check
                item.stock_last_check = timezone.now()
                item.stock = stock
                item.stock_days = stock_days
                result['stock_i'] = avail
                item.save(update_fields=['stock', 'stock_days',
                                         'stock_last_check', 'is_active'])
    return result

def check_stock(request, item_id):
    result = check_stock_f(item_id)
    js = json.dumps(result)
    return HttpResponse(js, content_type='application/json')


def payment_push(request):
    try:
        if request.method == "GET":
            form = PaymentSuccessForm(request.GET)
            if form.is_valid():
                inst = form.save(commit=False)
                inst.server_ip = get_client_ip(request)
                inst.save()
                pk_order = inst.MERCHANT_ORDER_ID.split("_")[0]
                ol_pk = inst.MERCHANT_ORDER_ID.split("_")[1]
                order = Order.objects.get(pk=int(pk_order))
                if order.coupon and order.is_payed == False:  # in case we already processed it 
                    order.coupon.used_count += 1
                    order.coupon.save(update_fields=['used_count',])
                if order.money_coupon and order.is_payed == False: # in case we already processed it
                    order.money_coupon.amount =  order.money_coupon.amount - order.coupon_delta
                    order.money_coupon.spended = order.coupon_delta
                    order.money_coupon.save()
                
                order.is_payed = True
                try:
                    order.total_payed = int(inst.AMOUNT)
                except:
                    print (traceback.format_exc())
                    order.total_payed = 0
                order.save(update_fields=['is_payed', 'total_payed'])

                order.pay_date = datetime.now()
                order.save(update_fields=['pay_date',])
                order_log = OrderLog.objects.get(pk=ol_pk)
                order_log.is_payed = True
                order_log.save(update_fields=['is_payed',])
                try:
                    order_url = reverse('manager:order_info', kwargs={"order_pk":order.pk})
                except:
                    print ("Can't build order url")
                    print (traceback.format_exc())
                    order_url = "error builing order url"

                message = "New order paid: %s\n%s%s" % (order.price, settings.SITE_URL, order_url) 
                for m in settings.MANAGERS:
                    eo, c = EmailObject.objects.get_or_create(
                        subject='[FASTEK.LINK] New  Order paid %s ' % order.price, 
                        text=message, to=m[1])

                return HttpResponse("YES", content_type='text/plain')
            else:
                try:
                    p = PaymentPushError(error=form.errors, post_data=request.GET)
                    p.save()
                except:
                    print(traceback.format_exc())
                print(form.errors)
    except:
        print(traceback.format_exc())
        try:
            p = PaymentPushError(error=traceback.format_exc(), post_data=request.GET)
            p.save()
        except:
            print(traceback.format_exc())

    for m in settings.MANAGERS:
        eo, c = EmailObject.objects.get_or_create(
            subject='Fastek.link: error',
            text="There was a error on payment push", to=m[1])
    raise PermissionDenied


def check_payment(order_id):
    """
    Функция принимает на вход номер заказа и возвращает оплаченную сумму 
    или False в случае ошибки или если платеж не был проведен

    Доки API:
    Обязательные параметры:
    merchant_id 	ID Вашего магазина на сервисе Free-kassa.ru
    s 	Контрольная подпись MD5, формируется из вашего Merchant ID и Секретного слова 2, например

    md5('7012'.'secret')

    intid 	Номер заказа на сервисе Free-kassa.ru (необходимо использовать или intid или order_id)
    order_id 	Номер заказа магазина (необходимо использовать или intid или order_id)
    action 	check_order_status

    Пример ответа:

     <?xml version="1.0" encoding="UTF-8" ?>
         <root>
             <answer>info</answer>
             <desc>Order Inf</desc>
             <status>completed</status>
             <intid>165354</intid>
             <id>123</id>
             <date>2015-03-10 16:08:32</date>
             <amount>37</amount>
             <description>Payment #123</description>
             <email>user@email.ru</email>
        </root>


    """
    sign_str = "%s%s" % (settings.MERCHANT_ID, settings.SECRET_KEY2)
    m = hashlib.md5()
    m.update(sign_str)
    sign = m.hexdigest()
    api_url = "http://www.free-kassa.ru/api.php"
    attempts = 5
    success = False
    while not success and attempts > 0:
        r = requests.get(api_url, params={"merchant_id": settings.MERCHANT_ID,
                                          "s":sign, "order_id":order_id, 
                                          "action":"check_order_status"})
        try:
            r.raise_for_status()
        except requests.exceptions.HTTPError:
            attemts -= 1
            time.sleep(6 - attempts)
        else:
            success = True
    status = False
    completed = False
    if success:
        print(r.text)
        root = ET.fromstring(r.text)
        for child in root:
            if child.tag == "status":
                if child.text == "completed":
                    completed = True
            if child.tag == "amount":
                status = child.text
            print((child.tag, child.text))
        if not completed:
            status = False
    return status

@login_required
def payment_success(request):
    """Get the order, check payment and amount!"""
    user = request.user
    emailuser = user.emailuser
    s_user = SUser(request)

    basket = get_object_or_404(Basket, user=s_user.get_user)
    basket_items = BasketItem.objects.filter(basket=basket).delete()
    order = Order.objects.filter(user=emailuser).order_by("-id")[0]
    return render(request, "user/payment-success.html", {"order": order})
