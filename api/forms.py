# -*- coding: utf-8 -*-


import hashlib

from django import forms
from django.forms import ModelForm, Form
from django.contrib.auth.models import User
from django.conf import settings

from .models import *

class PaymentSuccessForm(ModelForm):
    """
    md5($_REQUEST['MERCHANT_ID'].':'.$_REQUEST['AMOUNT'].':secret2:'.$_REQUEST['MERCHANT_ORDER_ID'])
    """
    class Meta:
        model = PaymentSuccess
        exclude = ("added", "server_ip")

    def clean_MERCHANT_ID(self):
        try:
            merch_id = int(self.cleaned_data['MERCHANT_ID'])
        except ValueError:
            raise forms.ValidationError('Merchant ID Error')
        if merch_id != int(settings.MERCHANT_ID):
            raise forms.ValidationError('Wrong Merchant ID')
        return self.cleaned_data['MERCHANT_ID']

    def clean_SIGN(self):
        cd = self.cleaned_data
        sign_str = "%s:%s:%s:%s" % (cd['MERCHANT_ID'], cd['AMOUNT'], settings.SECRET_KEY2, cd['MERCHANT_ORDER_ID'])
        m = hashlib.md5()
        m.update(sign_str.encode('utf8'))
        if cd['SIGN'] != m.hexdigest():
            raise forms.ValidationError("Sign is invalid")
        return cd['SIGN']
