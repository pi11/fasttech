# -*- coding: utf-8 -*-


from django.contrib import admin
from .models import PaymentSuccess, PaymentPushError

class PaymentSuccessAdmin(admin.ModelAdmin):
    list_display = ("MERCHANT_ORDER_ID", "AMOUNT", "added")

admin.site.register(PaymentSuccess, PaymentSuccessAdmin)

class PaymentPushErrorAdmin(admin.ModelAdmin):
    pass

admin.site.register(PaymentPushError, PaymentPushErrorAdmin)
