from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'check-stock/(?P<item_id>\d+)/$', check_stock, name='check_stock'),

    url(r'push/$', payment_push),
    url(r'success/$', payment_success, name="payment_success"),

]
