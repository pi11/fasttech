# -*- coding: utf-8 -*-


from django.db import models

class PaymentSuccess(models.Model):
    """    
    MERCHANT_ID 	ID Вашего магазина
    AMOUNT 	Сумма заказа
    intid 	Номер операции Free-Kassa
    MERCHANT_ORDER_ID 	Ваш номер заказа
    P_EMAIL 	Email плательщика
    P_PHONE 	Телефон плательщика (если указан)
    CUR_ID 	ID электронной валюты, который был оплачен заказ (список валют)
    SIGN 	Подпись (методика формирования подписи в данных оповещения)
    us_key 	Дополнительные параметры с префиксом us_, переданные в форму оплаты 
    """
    MERCHANT_ID = models.IntegerField()
    AMOUNT = models.CharField(max_length=100)
    intid = models.CharField(max_length=100)
    MERCHANT_ORDER_ID = models.CharField(max_length=100)
    P_EMAIL = models.CharField(max_length=250, null=True, blank=True)
    CUR_ID = models.CharField(max_length=100)
    SIGN = models.CharField(max_length=100)

    added = models.DateTimeField(auto_now_add=True)
    server_ip = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):
        return str(self.MERCHANT_ORDER_ID)


class PaymentPushError(models.Model):
    post_data = models.TextField()
    error = models.TextField()
    added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.error
