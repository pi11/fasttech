## Online store - parser for the fasttech.com portal. 

This Django-based project consist of:

- Parsing fasttech.com for e-cigaretes products, photos and user's reviews
- Online shop with basket and checking product availability at the time when user opens product page.
- Online chat for user support with telegram integration.
- Payment system integration
- Automatic package tracking and informing users via SMS and Email
- Coupons for marketing programms. And refund (money) coupons.
- Automatic (google translate) and semi-automatic translations for product titles/descriptions
- Manager and Admin interface (including parsing statistics, email, sms sent, orders processing, etc)
- Simple knowledge database and CMS
- Fast search using Sphinx search engine, saving users search history for analysis.
- Searching and embeding Youtube videos reviews for products.

Example deployment: https://fasttech.scurra.space/
